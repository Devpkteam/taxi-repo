const express = require('express');
const mysql      = require('mysql');
const sql = require('../db.js');
const app = express();
const FCM = require('fcm-node');
const fcmPasajero = new FCM('AAAAXyvFaUw:APA91bFySdVTYItpgc1gBoxpgPyuK7csC1AWzq9YmASROXXOwpZN7YmhuBTc1rzOh90czRhxMZACGXD0PY1Bx7O9T9xppkCwzsxQunYSbIzyvDYaeoSHs5Rl63d9aoT2SDXOy7RndKCj');
const fcmConductor = new FCM('AAAA6e5PVfE:APA91bHtKGJb3gAXpmLSq_3wruyrSwKbZdQcePN4XURbnWb2Z7X74DRizNqTBF-2BeSWZ9o-JtNn5GT3EceERr-9PPGOBipFvsbtRyWNEgGfETJTO2LY1D6i_2Xgn6esbQ2fLtO3En9I');
const cron = require('node-cron');
const moment = require('moment')

const url_taskMap = {};
var task;


const notificacion = (token,id,title,body)=>{
    return new Promise((resolve,reject)=>{
        console.log('enviando alerta')
     let msg = {
         to: token,
         data:{
             trip_id: `${id}`
         },
         notification: {
             title: title,
             icon: "ic_launcher",
             body: body,
             sound: "default",
             click_action: "FCM_PLUGIN_ACTIVITY"
         }
     }
     fcmPasajero.send(msg, function(err, response){
         if (err) {
             console.log(err)
             reject(false);
         } else {
             console.log(response)
             resolve(true) ;
         }
     });
    });
 }
const notificacionC = (token,id,title,body)=>{
return new Promise((resolve,reject)=>{
    console.log('enviando alerta')
    let msg = {
        to: token,
        data:{
            trip_id: `${id}`
        },
        notification: {
            title: title,
            icon: "ic_launcher",
            body: body,
            sound: "default",
            click_action: "FCM_PLUGIN_ACTIVITY"
        }
    }
    fcmConductor.send(msg, function(err, response){
        if (err) {
            console.log(err)
            reject(false);
        } else {
            console.log(response)
            resolve(true) ;
        }
    });
});
}

const cronFunctions = (tokenfmcClient, tokenfmcDriver, id, hours,client,conductor) => {
    console.log("Cron function started")
    switch (hours) {
        case hours = 12:
            task = cron.schedule('* */9 * * *',()=>{
                notificacion(tokenfmcClient, id, "Falta 6 horas para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 6 horas para su viaje", "");
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,6,client,conductor);
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
            });
            break;
        case hours = 6:
            task = cron.schedule('* */3 * * *',()=>{
                notificacion(tokenfmcClient, id, "Falta 3 horas para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 3 horas para su viaje", "");
                
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,3,client, conductor);
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
            });
            break;
        case hours = 3:
            task = cron.schedule('* */2 * * *',()=>{
                notificacion(tokenfmcClient, id, "Falta 1 hora para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 1 hora para su viaje", "");
                
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,1,client, conductor);
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
            });
            break;
        case hours = 1:
            task = cron.schedule('*/30 * * * *',()=>{
                console.log(tokenfmcDriver)
                notificacion(tokenfmcClient, id, "Falta 30 minutos para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 30 minutos para su viaje", "");
                
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,0.5,client, conductor);
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
            });
            break;
        case hours = 0.5:
            task = cron.schedule('*/25 * * * *',()=>{
                notificacion(tokenfmcClient, id, "Falta 5 minutos para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 5 minutos para su viaje", "");
                
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,0,client, conductor);
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
            });
            break;
        case hours = 0:
            // 5 MINUTOS
            task = cron.schedule('*/4 * * * *',()=>{
                console.log(tokenfmcDriver)
                notificacion(tokenfmcClient, id, "Falta 1 minuto para su viaje", "");
                notificacionC(tokenfmcDriver, id, "Falta 1 minuto para su viaje", "");
                
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
                cronFunctions(tokenfmcClient,tokenfmcDriver, id,0.1, client, conductor);
            });
            break;
        case hours = 0.1:
            task = cron.schedule('*/1 * * * *',()=>{
                console.log(tokenfmcDriver)
                notificacion(tokenfmcClient, id, "Ingrese para iniciar su viaje", "");
                notificacionC(tokenfmcDriver, id, "Ingrese para iniciar su viaje", "");
                let ramdon = Math.random();
                url_taskMap[`${hours}-${client}-${ramdon}`] = task;
                let my_job = url_taskMap[`${hours}-${client}-${ramdon}`];
                my_job.stop();
                // Logica para que inicie el viaje
            });
            break;
    }
}

const changeStatusScheduled = id=>{
    sql.query(`UPDATE scheduled_turistrips set scheduled = "true" WHERE id = ${id}`,(err,response,fields)=>{
        if(err){console.log(err)}else{console.log(response)}
    });
}

setInterval(() => {
  
    let viajes_programados = [];//return new Promise(resolve => {})
    const query = ()=>{
        return new Promise(resolve=>{
            sql.query(`SELECT * FROM scheduled_turistrips WHERE scheduled IS NULL`,(err,response,fields)=>{
                if(err){console.log(err)}else{resolve(response)}
            });
        });
    }
    query();
    query().then((data)=>{
        for(x in data){
            let amoment = new Date(data[x].time);
            amoment = moment(amoment);
            amoment = moment(amoment).fromNow();
            switch (amoment) {
                case "in 21 hours":
                        if(data[x].driver !== null){
                            sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                                if(err){console.log(err)}else{
                                    sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                        notificacion(response[0].tokenfcm, data[x].id, "Falta 21 horas para su viaje", "");
                                        notificacionC(res[0].tokenfcm, data[x].id, "Falta 21 horas para su viaje", "");
                                        cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id, 21,data[x].client, data[x].driver);
                                        changeStatusScheduled(data[x].id);
                                    });
                                }
                            });
                        }
                    break;
                case "in 12 hours":
                        if(data[x].driver !== null){
                            sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}' AND user_login = '${data[x].driver}';`,(err,response,fields)=>{
                                if(err){console.log(err)}else{
                                    sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                        notificacion(response[0].tokenfcm, data[x].id, "Falta 12 horas para su viaje", "");
                                        notificacionC(res[0].tokenfcm, data[x].id, "Falta 12 horas para su viaje", "");
                                        cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id, 12,data[x].client, data[x].driver);
                                        changeStatusScheduled(data[x].id);
                                    });
                                }
                            });
                        }
                    break;
                case"in 6 hours":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta 6 horas para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta 6 horas para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id, 6,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                case "in 3 hours":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta 3 horas para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta 3 horas para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id, 3,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                case "in 1 hours":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta una hora para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta una hora para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id,1,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                case "in 30 minutes":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta 30 minutos para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta 30 minutos para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id,0.5,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                case "in 5 minutes":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta 5 minutos para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta 5 minutos para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id,0,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                case "in a minute":
                    if(data[x].driver !== null){
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                console.log(response)
                                sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].driver}';`,(err,res,fields)=>{
                                    notificacion(response[0].tokenfcm, data[x].id, "Falta un minuto para su viaje", "");
                                    notificacionC(res[0].tokenfcm, data[x].id, "Falta un minuto para su viaje", "");
                                    cronFunctions(response[0].tokenfcm, res[0].tokenfcm, data[x].id,0.1,data[x].client, data[x].driver);
                                    changeStatusScheduled(data[x].id);
                                });
                            }
                        });
                    }
                    break;
                default:
                    if(amoment.indexOf("ago") !== -1){
                        sql.query(`DELETE FROM scheduled_turistrips WHERE id = ${data[x].id};`,(err,response,fields)=>{
                            if(err){console.log(err)}else{console.log(response)}
                        });
                        sql.query(`SELECT * FROM wp_users WHERE user_login = '${data[x].client}';`,(err,response,fields)=>{
                            if(err){console.log(err)}else{
                                notificacion(response[0].tokenfcm, data[x].id, "Se a eliminado un viaje", "Su viaje a sido eliminado por expirar el tiempo limite para conseguir un conductor.");
                            }
                        });
                    }
                    console.log(amoment);
                    console.log(data[x].id);
                    break;
            }
        }
    })
}, 1000 * 5); // Multiplicarlo por 60

app.post('/',(req,res)=>{
    console.log("Starting cron functions with "+req.body.hours+" Hours to alarm client "+req.body.client);
    cronFunctions(parseInt(req.body.hours), req.body.client);
    res.json({status:200, msj: `Cron function started succesfully`, hours:`${req.body.hours}`});
});

module.exports = app;
