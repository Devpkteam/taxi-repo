
var express = require('express');
var mysql      = require('mysql');
var sql = require('../db.js');
var app = express();
var FCM = require('fcm-node');
var fcmPasajero = new FCM('AAAAXyvFaUw:APA91bFySdVTYItpgc1gBoxpgPyuK7csC1AWzq9YmASROXXOwpZN7YmhuBTc1rzOh90czRhxMZACGXD0PY1Bx7O9T9xppkCwzsxQunYSbIzyvDYaeoSHs5Rl63d9aoT2SDXOy7RndKCj');





var gcm = require('node-gcm');
 
// Set up the sender with your GCM/FCM API key (declare this once for multiple messages)
// var sender = new gcm.Sender('');
 
// Prepare a message to be sent
var message = new gcm.Message({
    data: { key1: 'msg1' },
    notification: {
        title: "Hello, World",
        icon: "ic_launcher",
        body: "This is a notification that will be displayed if your app is in the background."
    }
});
 


// Specify which registration IDs to deliver the message to

 
// Actually send the message



app.get('/conductores',(req,res)=>{
    sql.query('SELECT * FROM wp_users WHERE user_status = 2 and lat IS NOT NULL and lng IS NOT NULL;',(err,conductores,fields)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            
            res.status(200).json({
                ok:true,
                conductores
            })
        }
    })
})

app.post('/conductor/position/:id',(req,res)=>{


    sql.query(`UPDATE wp_users SET lat = ${req.body.lat}, lng = ${req.body.lng} WHERE ID = ${req.params.id};`,(err,result,fields)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{           
            res.status(200).json({
                ok:true
            })
        }
    })
})

app.put('/pasajero/position/:id',(req,res)=>{


    sql.query(`UPDATE wp_users SET lat = ${req.body.lat}, lng = ${req.body.lng} WHERE ID = ${req.params.id};`,(err,result,fields)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
        
       

            res.status(200).json({
                ok:true
            })
        }
    })
})


app.post('/reportar/conductor',(req,res)=>{
    sql.query(`UPDATE wp_users SET alert = "true" WHERE user_login = '${req.body.user_login}'`,(err,response,fields)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            res.status(200).json({
                ok:true
            })
        }
    })
})

app.post('/alertar/cliente',(req,res)=>{
    console.log('alertando')
    sql.query(`SELECT * FROM taxi_trips WHERE id = ${req.body.id}`,(err,resp,fields)=>{
       
        if(err){
            res.status(200).json({
                ok:false
            })
        }else{
            let user_login = resp[0].client;
            sql.query(`SELECT * FROM wp_users WHERE user_login = '${user_login}'`,(err,resp,fields)=>{
                if(err){
                    res.status(200).json({
                        ok:false
                    })
                }else{
                    alertPasajeroViajeAceptado(resp[0].tokenfcm,req.body.id).then((resp)=>{
                        res.status(200).json({
                            ok:true
                        })
                    }).catch((err)=>{
                        res.status(200).json({
                            ok:false
                        })
                    })
                }
            })
        }
    })
})


function alertPasajeroViajeAceptado(token,id){
    return new Promise((resolve,reject)=>{
        console.log('enviando alerta')
 
     let msg = {
         to: token,
         data:{
             trip_id: `${id}`,
             type: 'viaje_aceptado'
         },
         priority: 'high',
         notification: {
             title: "Viaje aceptado",
             icon: "ic_launcher",
             body: "Un conductor ha aceptado su solicitud de viaje, presione para mas informacion",
             sound: "default",
             click_action: "FCM_PLUGIN_ACTIVITY"
         }
     }
     fcmPasajero.send(msg, function(err, response){
         if (err) {
             console.log(err)
             reject(false);
         } else {
             console.log(response)
             resolve(true) ;
         }
     });
    
    })
     
 }

app.post('/distancia',(req,res)=>{
    
    res.status(200).json({
        km:getKilometros(req.body.lat1,req.body.lon1,req.body.lat2,req.body.lon2)
    })
   
})

app.post('/fmctoken/:id',(req,res)=>{
    sql.query(`UPDATE wp_users SET tokenfcm = '${req.body.token}' WHERE ID = '${req.params.id}';`,(err,response,fields)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err:{code:1},
                msg: 'Ocurrio un error al intentar asignar un token de fcm al usuario'
            })
        }else{
            res.status(200).json({
                ok:true
            })
        }
    })
})

getKilometros = function(lat1,lon1,lat2,lon2)
{
rad = function(x) {return x*Math.PI/180;}
var R = 6378.137; //Radio de la tierra en km
var dLat = rad( lat2 - lat1 );
var dLong = rad( lon2 - lon1 );
var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
var d = R * c;
return d.toFixed(3); //Retorna tres decimales
}




module.exports = app;