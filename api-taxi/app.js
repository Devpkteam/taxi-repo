const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
const app = express();

app.use(cors({ origin: "*" }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header('Access-Control-Allow-Credentials', false);
    next();
});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

const usuarioRoutes = require('./routes/usuario');
const viajeRoutes = require('./routes/viaje');
const avisosRoutes = require('./routes/avisos');

app.use('/usuario', usuarioRoutes);
app.use('/viaje', viajeRoutes);
app.use('/avisos', avisosRoutes);

if(process.argv.indexOf('--prod') !== -1){
    app.listen(3000,'167.172.136.188', () => {
        console.log('Express server puerto 3000, --prod: \x1b[32m%s\x1b[0m', 'online')
    });
}else{
    app.listen(3000, () => {
        console.log('Express server puerto 3000: \x1b[32m%s\x1b[0m', 'online');
    });
}

