-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 21-03-2020 a las 17:20:23
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `youland_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codes`
--

CREATE TABLE `codes` (
  `id` int(11) NOT NULL,
  `trip_id` int(10) NOT NULL,
  `trip_type` varchar(256) NOT NULL,
  `cod_init` varchar(256) NOT NULL,
  `cod_end` varchar(256) NOT NULL,
  `dri_cod_init` varchar(5) NOT NULL,
  `dri_cod_end` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `codes`
--

INSERT INTO `codes` (`id`, `trip_id`, `trip_type`, `cod_init`, `cod_end`, `dri_cod_init`, `dri_cod_end`) VALUES
(10, 17, 'taxi', 'IIyN', 'rBQp', 'true', 'true'),
(11, 18, 'taxi', 'eCEG', 'GSzN', 'true', 'true'),
(12, 19, 'taxi', 'BTQD', 'BzU7', 'true', 'true'),
(13, 20, 'taxi', 'kEzI', 'w5EZ', 'true', 'true'),
(14, 21, 'taxi', 'IZ7e', 'e41v', 'true', 'true'),
(15, 22, 'taxi', 'r2gz', 'ABje', 'true', 'true'),
(16, 23, 'taxi', 'aWMh', 'HYII', 'true', 'true'),
(17, 24, 'taxi', 'YmY2', '1dKo', 'false', 'false'),
(18, 25, 'taxi', '9gPN', 'Wz43', 'false', 'false'),
(19, 26, 'taxi', 'Wsp8', '50Ws', 'false', 'false'),
(20, 27, 'taxi', 'C730', 'PXD7', 'false', 'false'),
(21, 28, 'taxi', 'mJlm', '9MfC', 'false', 'false'),
(22, 29, 'taxi', 'SGjc', '3EyL', 'true', 'true'),
(23, 30, 'taxi', 'HH4A', 'AzFv', 'false', 'false'),
(24, 31, 'taxi', 'IeYs', 'HSo9', 'true', 'false'),
(25, 32, 'taxi', 'Swg6', 'lsmy', 'true', 'true'),
(26, 33, 'taxi', 'nVIC', 'PLIa', 'false', 'false'),
(27, 34, 'taxi', '6VQP', '3YzI', 'false', 'false'),
(28, 35, 'taxi', 'zA7G', '5DLv', 'false', 'false'),
(29, 36, 'taxi', 'AL7i', '2biU', 'false', 'false'),
(30, 37, 'taxi', 'a0d8', 'gmE7', 'false', 'false'),
(31, 38, 'taxi', 'i5yp', 'hCTE', 'false', 'false'),
(32, 39, 'taxi', 'YsDD', 'WAYY', 'false', 'false'),
(33, 40, 'taxi', 'KC38', 'F1rQ', 'false', 'false'),
(34, 41, 'taxi', 'rCDI', '8dj8', 'false', 'false'),
(35, 42, 'taxi', 'FwZA', 'Xlib', 'false', 'false'),
(36, 43, 'taxi', 'qufT', '7kF9', 'false', 'false'),
(37, 44, 'taxi', 'vVlm', 'hLSA', 'false', 'false'),
(38, 45, 'taxi', 'QPD5', 'bz5n', 'false', 'false'),
(39, 46, 'taxi', 'MYZc', 'GqYB', 'false', 'false'),
(40, 47, 'taxi', '3bss', 'VGSc', 'false', 'false'),
(41, 48, 'taxi', 'yMvU', 'QMDH', 'false', 'false'),
(42, 49, 'taxi', 'l6OD', 'OTR9', 'false', 'false'),
(43, 50, 'taxi', '9nKH', '7K63', 'false', 'false'),
(44, 51, 'taxi', '1b9y', 'v002', 'false', 'false'),
(45, 52, 'taxi', '4FVb', 'Hr9y', 'false', 'false'),
(46, 53, 'taxi', '1Av5', '3Gxq', 'false', 'false'),
(47, 54, 'taxi', 'UQKf', 'rpt1', 'false', 'false'),
(48, 55, 'taxi', 'l8if', 'NssI', 'false', 'false'),
(49, 56, 'taxi', 'PjpP', 'd99P', 'false', 'false'),
(50, 57, 'taxi', 'jnV7', 'Gb8A', 'false', 'false'),
(51, 58, 'taxi', 'kgON', 'dVK7', 'false', 'false'),
(52, 59, 'taxi', 'fBVG', 'UADp', 'false', 'false'),
(53, 60, 'taxi', 'LnK3', '7JNe', 'false', 'false'),
(54, 61, 'taxi', 'STlQ', 'gp3W', 'false', 'false'),
(55, 62, 'taxi', '3E9x', 'Lcwp', 'false', 'false'),
(56, 63, 'taxi', 'ZOfb', 'VkRu', 'false', 'false'),
(57, 64, 'taxi', 'hFpZ', 'O4tY', 'false', 'false'),
(58, 65, 'taxi', 'fXIN', 'QzBy', 'false', 'false'),
(59, 66, 'taxi', 'OaEu', 'LUOj', 'false', 'false'),
(60, 67, 'taxi', 'w8Ty', 'Tzto', 'false', 'false'),
(61, 68, 'taxi', 'l0IU', '0muw', 'false', 'false'),
(62, 69, 'taxi', '8aIP', '8Af1', 'false', 'false'),
(63, 70, 'taxi', 'FrXg', 'OG3P', 'false', 'false'),
(64, 71, 'taxi', 'dQ46', 'tdUl', 'false', 'false'),
(65, 72, 'taxi', 'x3VC', 'ElSz', 'false', 'false'),
(66, 73, 'taxi', 'jQof', '1iXV', 'false', 'false'),
(67, 74, 'taxi', 'b7xz', '078U', 'false', 'false'),
(68, 75, 'taxi', 'hCD6', '9E3M', 'false', 'false'),
(69, 76, 'taxi', 'Geev', 'qgE3', 'false', 'false'),
(70, 77, 'taxi', 'TKh5', 'xvr0', 'false', 'false'),
(71, 78, 'taxi', 'wNsp', 'jZuA', 'false', 'false'),
(72, 79, 'taxi', 'MO45', '6TV0', 'false', 'false'),
(73, 80, 'taxi', 'SOjP', 'nshf', 'false', 'false'),
(74, 81, 'taxi', 'ZhKS', 'cUQb', 'false', 'false'),
(75, 82, 'taxi', 'MHqw', 'cQCR', 'false', 'false'),
(76, 83, 'taxi', 'YIi5', 'rmhD', 'false', 'false'),
(77, 84, 'taxi', 'FZ8w', '6QRc', 'false', 'false'),
(78, 85, 'taxi', '1xmH', 'p1nQ', 'false', 'false'),
(79, 86, 'taxi', 'S2Ed', '2n8Z', 'false', 'false'),
(80, 87, 'taxi', 'egAK', 'agEq', 'false', 'false'),
(81, 88, 'taxi', 'ldj5', 'e7dp', 'false', 'false'),
(82, 89, 'taxi', 'XU8s', 'El4v', 'false', 'false'),
(83, 90, 'taxi', 'HK0l', 'rrMt', 'false', 'false'),
(84, 91, 'taxi', 'IbzE', 'APPH', 'false', 'false'),
(85, 92, 'taxi', 'hE4k', 'squ1', 'false', 'false'),
(86, 93, 'taxi', 'VNVq', 'Ne2q', 'false', 'false'),
(87, 94, 'taxi', 'W5OY', 'gdrN', 'false', 'false'),
(88, 95, 'taxi', '4XSU', 'F6US', 'false', 'false'),
(89, 96, 'taxi', 'HBOS', 'rGxe', 'false', 'false'),
(90, 97, 'taxi', 'imvg', 'lmhv', 'false', 'false'),
(91, 98, 'taxi', '795l', 'jy5t', 'false', 'false'),
(92, 99, 'taxi', 'hdbK', 'vEVH', 'false', 'false'),
(93, 100, 'taxi', 'IiDL', 'lWAU', 'false', 'false'),
(94, 101, 'taxi', 'HHyR', 'Mepj', 'false', 'false'),
(95, 102, 'taxi', 'kdhJ', 'c2Ym', 'false', 'false'),
(96, 103, 'taxi', 'k3pq', '0UHI', 'false', 'false'),
(97, 104, 'taxi', 'MaP2', 'QA1I', 'false', 'false'),
(98, 105, 'taxi', 'NnhY', 'FdKV', 'false', 'false'),
(99, 106, 'taxi', 'DeMq', 'dgc0', 'false', 'false'),
(100, 107, 'taxi', 'GTSy', 'zBxD', 'false', 'false'),
(101, 108, 'taxi', 'CD6A', '87PW', 'false', 'false'),
(102, 109, 'taxi', 'k3IB', 'g5Wl', 'false', 'false'),
(103, 110, 'taxi', 'Diwi', 'UZOU', 'false', 'false'),
(104, 111, 'taxi', '0pel', 'iyBG', 'false', 'false'),
(105, 112, 'taxi', 'pfxJ', 'IRZa', 'false', 'false'),
(106, 113, 'taxi', 'I4JK', 'gesF', 'false', 'false'),
(107, 114, 'taxi', 'kIRG', 'w4Nx', 'false', 'false'),
(108, 115, 'taxi', 'yMxm', '3ufU', 'false', 'false'),
(109, 116, 'taxi', 'La3E', '8H4O', 'false', 'false'),
(110, 117, 'taxi', 'XHPJ', 'yS85', 'false', 'false'),
(111, 118, 'taxi', 'fRbg', 'HCJs', 'false', 'false'),
(112, 119, 'taxi', 'QIH2', 'PS8d', 'false', 'false'),
(113, 120, 'taxi', '3AM7', 'KBYf', 'false', 'false'),
(114, 121, 'taxi', 'cNVN', 'oMzq', 'false', 'false'),
(115, 122, 'taxi', 'waKZ', 'ChA7', 'false', 'false'),
(116, 123, 'taxi', 'yL9t', 'OJFD', 'false', 'false'),
(117, 124, 'taxi', 'avnA', '1vZP', 'false', 'false'),
(118, 125, 'taxi', 'AmFu', '2LYv', 'false', 'false'),
(119, 126, 'taxi', 'I6xB', 'CoAS', 'false', 'false'),
(120, 127, 'taxi', 'uQfP', 'XVZC', 'false', 'false'),
(121, 128, 'taxi', 'aEqi', 'FdxO', 'false', 'false'),
(122, 129, 'taxi', 'Nw0X', 'cJvI', 'false', 'false'),
(123, 130, 'taxi', 'LbEq', 'k6SN', 'false', 'false'),
(124, 131, 'taxi', 'YJZD', 'c3xt', 'false', 'false'),
(125, 132, 'taxi', 'LgJc', '87ru', 'false', 'false'),
(126, 133, 'taxi', 'ymCF', '1U1B', 'false', 'false'),
(127, 134, 'taxi', 'P5oX', 'N10Z', 'false', 'false'),
(128, 135, 'taxi', 'YQ8N', 'lqB6', 'false', 'false'),
(129, 136, 'taxi', 'fSw7', 'K1FP', 'false', 'false'),
(130, 137, 'taxi', 'zBzL', 'N1QA', 'false', 'false'),
(131, 138, 'taxi', 'KmAC', 'HxC8', 'false', 'false'),
(132, 139, 'taxi', 'oE3F', 'IinK', 'false', 'false'),
(133, 140, 'taxi', 'FhDK', 'PoaQ', 'false', 'false'),
(134, 141, 'taxi', '5Y7i', 'HdhY', 'false', 'false'),
(135, 142, 'taxi', 'pzEo', 'NPFO', 'false', 'false'),
(136, 143, 'taxi', '9tpw', 'bZtO', 'false', 'false'),
(137, 144, 'taxi', 'Kuec', 'v81J', 'false', 'false'),
(138, 145, 'taxi', 'fjJO', 'nP3F', 'false', 'false'),
(139, 146, 'taxi', 'p9kY', 'abRo', 'false', 'false'),
(140, 147, 'taxi', 'RvhC', 'mTGy', 'false', 'false'),
(141, 148, 'taxi', 'Pre1', 'diL7', 'false', 'false');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scheduled_turistrips`
--

CREATE TABLE `scheduled_turistrips` (
  `viaje_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `client` varchar(256) NOT NULL,
  `time` varchar(256) NOT NULL,
  `driver` varchar(256) DEFAULT NULL,
  `precio` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taxi_trips`
--

CREATE TABLE `taxi_trips` (
  `id` int(11) NOT NULL,
  `client` varchar(256) NOT NULL,
  `time` varchar(256) NOT NULL,
  `driver` varchar(256) DEFAULT NULL,
  `precio` int(100) NOT NULL,
  `startID` varchar(1000) DEFAULT NULL,
  `endID` varchar(1000) DEFAULT NULL,
  `alert` varchar(5) NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `taxi_trips`
--

INSERT INTO `taxi_trips` (`id`, `client`, `time`, `driver`, `precio`, `startID`, `endID`, `alert`) VALUES
(132, 'cliente@gmail.com', 'Fri Mar 20 2020 16:11:15 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(133, 'cliente@gmail.com', 'Fri Mar 20 2020 16:14:23 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(134, 'cliente@gmail.com', 'Fri Mar 20 2020 16:15:34 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(135, 'cliente@gmail.com', 'Fri Mar 20 2020 16:18:36 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(136, 'cliente@gmail.com', 'Fri Mar 20 2020 16:19:42 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(137, 'cliente@gmail.com', 'Fri Mar 20 2020 16:21:12 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(138, 'cliente@gmail.com', 'Fri Mar 20 2020 16:22:56 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(139, 'cliente@gmail.com', 'Fri Mar 20 2020 16:24:09 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(140, 'cliente@gmail.com', 'Fri Mar 20 2020 16:46:36 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(141, 'cliente@gmail.com', 'Fri Mar 20 2020 16:58:41 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(142, 'cliente@gmail.com', 'Fri Mar 20 2020 17:00:11 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(143, 'cliente@gmail.com', 'Fri Mar 20 2020 17:23:26 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(144, 'cliente@gmail.com', 'Fri Mar 20 2020 17:24:20 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(145, 'cliente@gmail.com', 'Fri Mar 20 2020 17:25:29 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(146, 'cliente@gmail.com', 'Fri Mar 20 2020 17:35:34 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(147, 'cliente@gmail.com', 'Fri Mar 20 2020 17:36:09 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false'),
(148, 'cliente@gmail.com', 'Fri Mar 20 2020 17:36:57 GMT-0400 (hora de Venezuela)', 'conductor@gmail.com', 59, 'Proyecto Kamila, Caracas, Miranda, Venezuela', 'Plaza Venezuela, Avenida Casanova, Caracas, Distrito Capital, Venezuela', 'false');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallet_history`
--

CREATE TABLE `wallet_history` (
  `id` int(11) NOT NULL,
  `user` varchar(250) NOT NULL,
  `monto` int(111) NOT NULL,
  `tipo` varchar(256) NOT NULL,
  `fecha` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `wallet_history`
--

INSERT INTO `wallet_history` (`id`, `user`, `monto`, `tipo`, `fecha`) VALUES
(1, 'cliente@gmail.com', 151, 'Viaje Taxi', '1582033346360'),
(2, 'cliente@gmail.com', 0, 'Viaje Turistico', '1582033995613'),
(3, 'cliente@gmail.com', 0, 'Viaje Turistico', '1582034132365'),
(4, 'cliente@gmail.com', 28, 'Viaje Taxi', '1582203853067'),
(5, 'cliente@gmail.com', 10, 'Viaje Taxi', '1582303477779'),
(6, 'cliente@gmail.com', 19, 'Viaje Taxi', '1582305493855'),
(7, 'cliente@gmail.com', 0, 'Viaje Turistico', '1582305556502'),
(8, 'cliente@gmail.com', 19, 'Viaje Taxi', '1582305732178'),
(9, 'cliente@gmail.com', 14, 'Recarga', '1582307418916'),
(10, 'cliente@gmail.com', 0, 'Viaje Turistico', '1582307446215'),
(11, 'cliente@gmail.com', 19, 'Viaje Taxi', '1582307518902'),
(12, 'cliente@gmail.com', 20, 'Recarga', '1582308150383'),
(13, 'cliente@gmail.com', 0, 'Viaje Turistico', '1582308249213'),
(14, 'cliente@gmail.com', 19, 'Viaje Taxi', '1582308387651'),
(15, 'cliente@gmail.com', 19, 'Viaje Taxi', '1582308610731'),
(16, 'davidvalor@gmail.com', 40, 'Viaje Taxi', '1583159638177'),
(17, 'davidvalor@gmail.com', 40, 'Viaje Taxi', '1583159816549'),
(18, 'davidvalor@gmail.com', 40, 'Viaje Taxi', '1583159845548'),
(19, 'davidvalor@gmail.com', 0, 'Viaje Turistico', '1583159871874'),
(20, 'davidvalor@gmail.com', 0, 'Viaje Turistico', '1583159995195'),
(21, 'davidvalor@gmail.com', 234, 'Recarga', '1583160056921'),
(22, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584368834499'),
(23, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584369148963'),
(24, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584369229136'),
(25, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584369347300'),
(26, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584372513734'),
(27, 'cliente@gmail.com', 37, 'Viaje Taxi', '1584381586400'),
(28, 'cliente@gmail.com', 27, 'Viaje Taxi', '1584382009359'),
(29, 'cliente@gmail.com', 27, 'Viaje Taxi', '1584382053081'),
(30, 'cliente@gmail.com', 110, 'Viaje Taxi', '1584456447379'),
(31, 'cliente@gmail.com', 113, 'Viaje Taxi', '1584457167190'),
(32, 'cliente@gmail.com', 10, 'Viaje Taxi', '1584457238448'),
(33, 'cliente@gmail.com', 10, 'Viaje Taxi', '1584457304505'),
(34, 'cliente@gmail.com', 113, 'Viaje Taxi', '1584458930559'),
(35, 'cliente@gmail.com', 113, 'Viaje Taxi', '1584459095807'),
(36, 'cliente@gmail.com', 113, 'Viaje Taxi', '1584459381578'),
(37, 'cliente@gmail.com', 1000000000, 'Recarga', '1584459543638'),
(38, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584459571298'),
(39, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584459629582'),
(40, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584459872752'),
(41, 'cliente@gmail.com', 113, 'Viaje Taxi', '1584460095967'),
(42, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584480139414'),
(43, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584486862399'),
(44, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584487517816'),
(45, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584487601026'),
(46, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584489298671'),
(47, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584489348009'),
(48, 'cliente@gmail.com', 28, 'Viaje Taxi', '1584491362195'),
(49, 'cliente@gmail.com', 145, 'Viaje Taxi', '1584491879663'),
(50, 'cliente@gmail.com', 2255555, 'Recarga', '1584492127947'),
(51, 'cliente@gmail.com', 27, 'Viaje Taxi', '1584495083428'),
(52, 'cliente@gmail.com', 27, 'Viaje Taxi', '1584495339998'),
(53, 'cliente@gmail.com', 27, 'Viaje Taxi', '1584495745183'),
(54, 'conductor@gmail.com', 258, 'Solicitud Retiro', '1584496322589'),
(55, 'conductor@gmail.com', 66655, 'Solicitud Retiro', '1584496326669'),
(56, 'cliente@gmail.com', 54, 'Viaje Taxi', '1584549030546'),
(57, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581019369'),
(58, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581091687'),
(59, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581110576'),
(60, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581288051'),
(61, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581312903'),
(62, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581330603'),
(63, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581367132'),
(64, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581407098'),
(65, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581578487'),
(66, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584581621160'),
(67, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584639314396'),
(68, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584639608087'),
(69, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584639717681'),
(70, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584639813669'),
(71, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584640943958'),
(72, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584642696334'),
(73, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584642727607'),
(74, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643037611'),
(75, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643294901'),
(76, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643318831'),
(77, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643336506'),
(78, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643587361'),
(79, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643601616'),
(80, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643707415'),
(81, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643881532'),
(82, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584643912517'),
(83, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644004946'),
(84, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644037077'),
(85, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644086544'),
(86, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644407175'),
(87, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644470605'),
(88, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584644571707'),
(89, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584653363261'),
(90, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584653430984'),
(91, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584654016590'),
(92, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584654201259'),
(93, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656539438'),
(94, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656633202'),
(95, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656649366'),
(96, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656670849'),
(97, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656680334'),
(98, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656708675'),
(99, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656793063'),
(100, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656800550'),
(101, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656826764'),
(102, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584656912407'),
(103, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584657601049'),
(104, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584657617760'),
(105, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584657643996'),
(106, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584657659626'),
(107, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584657827498'),
(108, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584658436208'),
(109, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584658456332'),
(110, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584660225299'),
(111, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584660231714'),
(112, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584660347229'),
(113, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584660363184'),
(114, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584660452097'),
(115, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661179267'),
(116, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661190645'),
(117, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661205684'),
(118, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661218950'),
(119, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661534731'),
(120, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584661558076'),
(121, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584664194728'),
(122, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584665082100'),
(123, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584665158032'),
(124, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584665656207'),
(125, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584666410921'),
(126, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584666422614'),
(127, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584666432693'),
(128, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584720941943'),
(129, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584721753222'),
(130, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584721765372'),
(131, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584721959771'),
(132, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584722121405'),
(133, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584722209898'),
(134, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584724067218'),
(135, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584724099384'),
(136, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584724122783'),
(137, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584724151539'),
(138, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725139487'),
(139, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725704132'),
(140, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725724510'),
(141, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725751560'),
(142, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725762797'),
(143, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725770053'),
(144, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584725915594'),
(145, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584726889312'),
(146, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584726902123'),
(147, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584726939552'),
(148, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584726978136'),
(149, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584726988650'),
(150, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584727075066'),
(151, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584727105885'),
(152, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584727157003'),
(153, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735076125'),
(154, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735264087'),
(155, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735335062'),
(156, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735516745'),
(157, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735583106'),
(158, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735673494'),
(159, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735776979'),
(160, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584735849879'),
(161, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584737196908'),
(162, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584737922370'),
(163, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584738012636'),
(164, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584739406987'),
(165, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584739461599'),
(166, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584739530597'),
(167, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584740138238'),
(168, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584740170543'),
(169, 'cliente@gmail.com', 59, 'Viaje Taxi', '1584740218050');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_end` date NOT NULL,
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `date_end`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(9, 0, '2020-01-17 10:37:20', '2020-01-17 19:30:00', '0000-00-00', '[{\"name\":\"José María Pino Suárez José María Pino Suárez 30, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06060 Ciudad de México, CDMX, México\",\"lat\":19.429241386305847,\"lng\":-99.13217067718506},{\"name\":\"Rosario 156, La Merced, Zona Centro, Venustiano Carranza, 15100 Ciudad de México, CDMX, México\",\"lat\":19.42661072109788,\"lng\":-99.12328720092773}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 14:37:19', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(10, 0, '2020-01-17 10:46:56', '2020-01-17 06:30:00', '0000-00-00', '[{\"name\":\"Rosario 156, La Merced, Zona Centro, Venustiano Carranza, 15100 Ciudad de México, CDMX, México\",\"lat\":19.42661072109788,\"lng\":-99.12328720092773},{\"name\":\"Rosario 156, La Merced, Zona Centro, Venustiano Carranza, 15100 Ciudad de México, CDMX, México\",\"lat\":19.42661072109788,\"lng\":-99.12328720092773},{\"name\":\"República de El Salvador 95, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06000 Centro, CDMX, México\",\"lat\":19.42928185774553,\"lng\":-99.13500308990479}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 14:46:56', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(11, 0, '2020-01-17 11:03:17', '2020-01-17 18:24:00', '0000-00-00', '[{\"name\":\"Morelos, Morelos, 15270 Ciudad de México, CDMX, México\",\"lat\":19.43972315222889,\"lng\":-99.11818027496338},{\"name\":\"Av. Hidalgo 45, Centro Histórico de la Cdad. de México, Guerrero, Cuauhtémoc, 06300 Ciudad de México, CDMX, México\",\"lat\":19.437092656807508,\"lng\":-99.14332866668701},{\"name\":\"República de El Salvador 95, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06000 Centro, CDMX, México\",\"lat\":19.42928185774553,\"lng\":-99.13500308990479},{\"name\":\"Av. Morelos 67, Juárez, Cuauhtémoc, 06600 Ciudad de México, CDMX, México\",\"lat\":19.433005187056306,\"lng\":-99.15174007415771}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 15:03:17', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(12, 0, '2020-01-17 11:05:26', '2020-01-17 04:21:00', '0000-00-00', '[{\"name\":\"Plaza de la Constitución 29, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.43247906961478,\"lng\":-99.13122653961182},{\"name\":\"Av. Hidalgo 45, Centro Histórico de la Cdad. de México, Guerrero, Cuauhtémoc, 06300 Ciudad de México, CDMX, México\",\"lat\":19.437092656807508,\"lng\":-99.14332866668701}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 15:05:25', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(13, 0, '2020-01-17 11:05:44', '2020-01-17 13:05:00', '0000-00-00', '[{\"name\":\"San Lázaro, 7 de Julio, 15390 Ciudad de México, CDMX, México\",\"lat\":19.43118400403601,\"lng\":-99.11418914794922},{\"name\":\"H.Congreso de la Unión 66, El Parque, Venustiano Carranza, 15960 Ciudad de México, CDMX, México\",\"lat\":19.430374582805314,\"lng\":-99.11792278289795}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 15:05:43', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(14, 0, '2020-01-17 11:06:05', '2020-01-17 18:00:00', '0000-00-00', '[{\"name\":\"Manuel de la Peña y Peña 14, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.4405729954864,\"lng\":-99.12676334381104},{\"name\":\"Plaza Garibaldi 43, Centro, Cuauhtémoc, 06000 Centro, CDMX, México\",\"lat\":19.440653932707455,\"lng\":-99.13955211639404}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 15:06:04', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(15, 0, '2020-01-17 16:38:30', '2020-01-17 12:00:00', '0000-00-00', '[{\"name\":\"Piso 4 Torre Privanza, Cto. Interior Melchor Ocampo 193, Verónica Anzúres, Miguel Hidalgo, 11300 Ciudad de México, CDMX, México\",\"lat\":19.435780902944835,\"lng\":-99.17057985290685},{\"name\":\"Heliotropo 131A, Atlampa, Cuauhtémoc, 06450 Ciudad de México, CDMX, México\",\"lat\":19.45876613032136,\"lng\":-99.16113847717443}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 20:38:31', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(16, 0, '2020-01-17 19:13:40', '2020-01-17 07:28:00', '0000-00-00', '[{\"name\":\"Calle Dr. Lucio 12, Doctores, Cuauhtémoc, 06720 Ciudad de México, CDMX, México\",\"lat\":19.425557207341075,\"lng\":-99.15108478784059},{\"name\":\"Av. Dr. Río de la Loza 69, Doctores, Cuauhtémoc, 06720 Ciudad de México, CDMX, México\",\"lat\":19.425112010742858,\"lng\":-99.14533413171266},{\"name\":\"Fray Servando Teresa de Mier 13, Centro, Cuauhtémoc, 06080 Ciudad de México, CDMX, México\",\"lat\":19.424423977237062,\"lng\":-99.13996971368287},{\"name\":\"Fray Servando Teresa de Mier 48, Centro, Cuauhtémoc, 06080 Ciudad de México, CDMX, México\",\"lat\":19.424100195755585,\"lng\":-99.13718021630739},{\"name\":\"José María Izazaga 80, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06080 Centro, CDMX, México\",\"lat\":19.426285708234012,\"lng\":-99.13726604699586},{\"name\":\"Eje 1A Sur 160, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06080 Ciudad de México, CDMX, México\",\"lat\":19.42584051363205,\"lng\":-99.13138664483522}]', 'Alameda Central', '', '0', 'open', 'open', '', '', '', '', '2020-01-17 23:13:41', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(17, 0, '2020-01-17 19:18:23', '2020-01-17 12:00:00', '0000-00-00', '[{\"name\":\"Av Chapultepec 530, Roma Nte., Cuauhtémoc, 06600 Ciudad de México, CDMX, México\",\"lat\":19.421025893829878,\"lng\":-99.17449741512452},{\"name\":\"Praga 58, Roma Nte., Cuauhtémoc, 06700 Ciudad de México, CDMX, México\",\"lat\":19.42224009403071,\"lng\":-99.16896133571778}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-01-17 23:18:24', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(18, 0, '2020-01-23 12:55:23', '2020-01-23 03:30:00', '0000-00-00', '[{\"name\":\"Museo Nacional de Antropología, Avenida Paseo de la Reforma, Polanco, Bosque de Chapultepec I Sección, Ciudad de México, CDMX, México\",\"lat\":19.4260032,\"lng\":-99.1862786},{\"name\":\"Casco Sto. Tomás., Calle Plan de Ayala, Plutarco Elías Calles, Ciudad de México, CDMX, México\",\"lat\":19.4529274,\"lng\":-99.17160229999999},{\"name\":\"Los Mochis, Sin., México\",\"lat\":25.7904657,\"lng\":-108.985882}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-01-23 16:55:23', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(19, 0, '2020-02-01 11:16:06', '2020-02-01 01:05:00', '0000-00-00', '[{\"name\":\"CARACAS RANAS RESTAURANT-BAR, Prolongación Paseo de los Heroes, Alfonso Coronadel Rosal, Tijuana, B.C., México\",\"lat\":32.5129869,\"lng\":-116.97587},{\"name\":\"Caracas, Alta Vista, Monterrey, N.L., México\",\"lat\":25.648381,\"lng\":-100.2867955},{\"name\":\"Aguascalientes, Ags., México\",\"lat\":21.8852562,\"lng\":-102.2915677},{\"name\":\"Baja California 81, Villa Milpa Alta, San Agustín, Milpa Alta, 12000 Villa Milpa Alta, CDMX, México\",\"lat\":19.187867547622375,\"lng\":-99.01399504670502},{\"name\":\"Aguascalientes Marriott Hotel, Trojes de Alonso, Aguascalientes, Ags., México\",\"lat\":21.9270674,\"lng\":-102.2904932},{\"name\":\"Aguascalientes, Ags., México\",\"lat\":21.8852562,\"lng\":-102.2915677},{\"name\":\"Col del Mar, Ciudad de México, CDMX, México\",\"lat\":19.2868303,\"lng\":-99.0598732},{\"name\":\"El Ángel de la Independencia, Avenida Paseo de la Reforma, Juárez, Ciudad de México, CDMX, México\",\"lat\":19.4270245,\"lng\":-99.16766469999999}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-02-01 15:16:10', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(20, 0, '2020-02-03 21:04:42', '2020-02-03 10:30:00', '0000-00-00', '[{\"name\":\"Estadio Azteca, Exejido de Sta Úrsula Coapa, 04910 Ciudad de México, CDMX, México\",\"lat\":19.301787406498097,\"lng\":-99.1470193862915},{\"name\":\"Anillo Periférico Canal de Garay - Parque Ecológico Xochimilco I, Parque Ecológico de Xochimilco, 16036 Ciudad de México, CDMX, México\",\"lat\":19.296805449321013,\"lng\":-99.09440517425537},{\"name\":\"Área Natural Protegida Bosque De Tlalpan, Carlos Lazo 362, Miguel Hidalgo 3ra Secc, Tlalpan, 14250 Ciudad de México, CDMX, México\",\"lat\":19.286881586759826,\"lng\":-99.2039680480957}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-02-04 03:04:35', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(21, 0, '2020-02-03 21:06:33', '2020-02-03 10:15:00', '0000-00-00', '[{\"name\":\"Manuel de la Peña y Peña 14, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.4405729954864,\"lng\":-99.12676334381104},{\"name\":\"Av. Hidalgo 45, Centro Histórico de la Cdad. de México, Guerrero, Cuauhtémoc, 06300 Ciudad de México, CDMX, México\",\"lat\":19.437092656807508,\"lng\":-99.14332866668701},{\"name\":\"Buenavista, México, Buenavista, 06350 Ciudad de México, CDMX, México\",\"lat\":19.44619803628396,\"lng\":-99.15246963500977}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-02-04 03:06:26', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(22, 0, '2020-02-05 08:45:29', '2020-02-05 10:30:00', '0000-00-00', '[{\"name\":\"José María Pino Suárez José María Pino Suárez 30, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06060 Ciudad de México, CDMX, México\",\"lat\":19.429241386305847,\"lng\":-99.13217067718506},{\"name\":\"Eje Central Lázaro Cárdenas 127, Colonia Centro, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.435231049679842,\"lng\":-99.14122581481934},{\"name\":\"Av. Hidalgo 51, Centro Histórico de la Cdad. de México, Guerrero, Cuauhtémoc, 06300 Ciudad de México, CDMX, México\",\"lat\":19.436971248297603,\"lng\":-99.14345741271973}]', 'Alameda Central', '', '-1', 'open', 'open', '', '', '', '', '2020-02-05 14:45:30', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(23, 0, '2020-02-05 10:51:57', '2020-02-05 10:30:00', '0000-00-00', '[{\"name\":\"La Bestia, Avenida José Vasconcelos, Colonia Condesa, Ciudad de México, CDMX, México\",\"lat\":19.4157295,\"lng\":-99.1793213},{\"name\":\"La Condesa, Ciudad de México, CDMX, México\",\"lat\":19.4149803,\"lng\":-99.17744619999999}]', 'Probando', '', '-1', 'open', 'open', '', '', '', '', '2020-02-05 14:51:58', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(24, 0, '2020-02-05 15:10:23', '2020-02-05 10:30:00', '0000-00-00', '[{\"name\":\"Luis Moya 14, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.434300238112474,\"lng\":-99.14457321166992},{\"name\":\"Museo de la Ciudad, Centro Histórico de la Cdad. de México, Centro, 06000 Ciudad de México, CDMX, México\",\"lat\":19.42928185774553,\"lng\":-99.1322135925293},{\"name\":\"La Bestia, Avenida José Vasconcelos, Colonia Condesa, Ciudad de México, CDMX, México\",\"lat\":19.4157295,\"lng\":-99.1793213}]', 'probando', '', '-1', 'open', 'open', '', '', '', '', '2020-02-05 19:10:23', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(25, 0, '2020-02-05 15:26:44', '2020-02-05 12:00:00', '0000-00-00', '[{\"name\":\"República de Brasil 10, Centro Histórico de la Cdad. de México, Centro, Cuauhtémoc, 06000 Ciudad de México, CDMX, México\",\"lat\":19.437456881792503,\"lng\":-99.13393020629883},{\"name\":\"Eje Central Lázaro Cárdenas S/N, Tlatelolco, Cuauhtémoc, 06900 Ciudad de México, CDMX, México\",\"lat\":19.451701484621466,\"lng\":-99.1377067565918},{\"name\":\"Tlatelolco, Tlatelolco, 06900 Ciudad de México, CDMX, México\",\"lat\":19.451084999444497,\"lng\":-99.1320605156956},{\"name\":\"Felipe Pescador, Ciudad de México, CDMX, México\",\"lat\":19.4540833,\"lng\":-99.1252086}]', 'nueva ruta', '', '-1', 'open', 'open', '', '', '', '', '2020-02-05 19:26:45', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0),
(26, 0, '2020-02-06 12:22:59', '2020-02-06 10:30:00', '0000-00-00', '[{\"name\":\"Hidalgo 82, Sta. Úrsula Coapa, Coyoacán, 04650 Ciudad de México, CDMX, México\",\"lat\":19.30822769150043,\"lng\":-99.14741783291016},{\"name\":\"Estadio Azteca, Calzada de Tlalpan, Sta. Úrsula Coapa, Ciudad de México, CDMX, México\",\"lat\":19.3028607,\"lng\":-99.1505277},{\"name\":\"Estadio Olímpico Universitario, Avenida de los Insurgentes Sur, Ciudad Universitaria, Ciudad de México, CDMX, México\",\"lat\":19.3319125,\"lng\":-99.1921777},{\"name\":\"Embarcadero Nuevo Nativitas Xochimilco, Calle del Mercado, San Jerónimo, Ciudad de México, CDMX, México\",\"lat\":19.2524331,\"lng\":-99.09364579999999},{\"name\":\"Museo Frida Kahlo, Londres, Del Carmen, Ciudad de México, CDMX, México\",\"lat\":19.3551806,\"lng\":-99.1624636}]', 'Ruta Sur', '', '-1', 'open', 'open', '', '', '', '', '2020-02-06 18:22:49', '0000-00-00 00:00:00', '', 0, '', 0, 'turis-trips', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  `document_front` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_back` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`, `document_front`, `document_back`) VALUES
(1, 1, 'md5', '30da7ce61c32f0e208d63ffcdabe055f', NULL, NULL),
(2, 1, 'md5', '30da7ce61c32f0e208d63ffcdabe055f', NULL, NULL),
(3, 6, 'md5', 'dfe2971e47a569579ed436f95d5336ca', NULL, NULL),
(4, 6, 'md5', 'dfe2971e47a569579ed436f95d5336ca', NULL, NULL),
(5, 8, 'cedula', 'pasaporte66656564', '1579879077111.jpg', '1579879082910.jpg'),
(6, 8, 'licencia', '', '1579879107888.jpg', '1579879120995.jpg'),
(7, 8, 'placa', '955584', '1579879166024.jpg', ''),
(8, 8, 'referencia', 'luis', '64654', 'luis'),
(9, 8, 'guia', '', '1579879131229.jpg', '1579879131229.jpg'),
(10, 8, 'referencia', 'luis', '423454544', 'kuis'),
(11, 8, 'identificacion', '', '1579879180903.jpg', '1579879120995.jpg'),
(12, 8, 'poliza', '959595', '1579879175478.jpg', ''),
(13, 9, 'md5', '0d843000989be1cd78a372d02c7a39a5', NULL, NULL),
(14, 9, 'md5', '0d843000989be1cd78a372d02c7a39a5', NULL, NULL),
(15, 11, 'md5', 'd9e2cfff639d4919dc643d2409ec6c69', NULL, NULL),
(16, 11, 'md5', 'd9e2cfff639d4919dc643d2409ec6c69', NULL, NULL),
(17, 12, 'md5', 'e0eeb1ecd530e33866e6dbf8f12e8ed5', NULL, NULL),
(18, 12, 'md5', 'e0eeb1ecd530e33866e6dbf8f12e8ed5', NULL, NULL),
(19, 13, 'md5', 'b4be43f978ddf49c6f48deef79131e2f', NULL, NULL),
(20, 13, 'md5', 'b4be43f978ddf49c6f48deef79131e2f', NULL, NULL),
(21, 14, 'referencia', 'guadalupe', '3456217890', 'av 2'),
(22, 14, 'guia', '', '1581012412201.jpg', '1581012412201.jpg'),
(23, 14, 'poliza', '345587568', '1581012685573.jpg', ''),
(24, 14, 'cedula', 'cedula12345677', '1581012467671.jpg', '1581012459331.jpg'),
(25, 14, 'referencia', 'lalo', '98765423', 'av 1'),
(26, 14, 'licencia', '', '1581012312660.jpg', '1581012340916.jpg'),
(27, 14, 'placa', '386_hgf', '1581012643066.jpg', ''),
(28, 14, 'identificacion', '', '1581012715111.jpg', '1581012340916.jpg'),
(29, 15, 'cedula', 'cedula1627394949', '1581023655191.jpg', '1581023696616.jpg'),
(30, 15, 'guia', '', '1581023853896.jpg', '1581023853896.jpg'),
(31, 15, 'licencia', '', '1581023779956.jpg', '1581023810825.jpg'),
(32, 15, 'referencia', 'carlos', '5544332211', 'uno dos'),
(33, 15, 'referencia', 'gabriel hernar', '6677880066', 'calle 5'),
(34, 15, 'poliza', '4465880086', '1581024081382.jpg', ''),
(35, 15, 'identificacion', '', '1581024145384.jpg', '1581023810825.jpg'),
(36, 15, 'placa', '456-uhl', '1581024036064.jpg', ''),
(37, 15, 'referencia', 'gabriel hernar', '6677880066', 'calle 5'),
(38, 15, 'licencia', '', '1581023779956.jpg', '1581023810825.jpg'),
(39, 15, 'poliza', '4465880086', '1581024081382.jpg', ''),
(40, 15, 'referencia', 'carlos', '5544332211', 'uno dos'),
(41, 15, 'placa', '456-uhl', '1581024036064.jpg', ''),
(42, 15, 'identificacion', '', '1581024145384.jpg', '1581023810825.jpg'),
(43, 15, 'cedula', 'cedula1627394949', '1581023655191.jpg', '1581023696616.jpg'),
(44, 15, 'guia', '', '1581023853896.jpg', '1581023853896.jpg'),
(45, 15, 'licencia', '', '1581023779956.jpg', '1581023810825.jpg'),
(46, 15, 'guia', '', '1581023853896.jpg', '1581023853896.jpg'),
(47, 15, 'referencia', 'carlos', '5544332211', 'uno dos'),
(48, 15, 'referencia', 'gabriel hernar', '6677880066', 'calle 5'),
(49, 15, 'cedula', 'cedula1627394949', '1581023655191.jpg', '1581023696616.jpg'),
(50, 15, 'identificacion', '', '1581024145384.jpg', '1581023810825.jpg'),
(51, 15, 'placa', '456-uhl', '1581024036064.jpg', ''),
(52, 15, 'poliza', '4465880086', '1581024081382.jpg', ''),
(53, 17, 'cedula', 'cedula1527374838', '1581024735306.jpg', '1581024800101.jpg'),
(54, 17, 'licencia', '', '1581024855060.jpg', '1581024901564.jpg'),
(55, 17, 'poliza', '44557899797', '1581025193882.jpg', ''),
(56, 17, 'referencia', 'alfa', '1155337788', 'calle 15'),
(57, 17, 'placa', 'wff-ahd', '1581025106143.jpg', ''),
(58, 17, 'guia', '', '1581024956348.jpg', '1581024956348.jpg'),
(59, 17, 'referencia', 'bravo', '6677112299', 'calle 6'),
(60, 17, 'identificacion', '', '1581025219408.jpg', '1581024901564.jpg'),
(61, 18, 'md5', 'bd8e1d55345d1055060ac886b14fb682', NULL, NULL),
(62, 18, 'md5', 'bd8e1d55345d1055060ac886b14fb682', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_photo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_identification` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime DEFAULT NULL,
  `user_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cash` int(11) NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tokenfcm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alert` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_photo`, `user_email`, `user_identification`, `user_registered`, `user_phone`, `user_status`, `display_name`, `cash`, `lat`, `lng`, `tokenfcm`, `alert`) VALUES
(1, 'admin@youland.com', '25d55ad283aa400af464c76d713c07ad', '', 'jose95ar@gmail.com', '0', '0000-00-00 00:00:00', '1', -1, 'jose', 0, NULL, NULL, NULL, 'false'),
(2, 'jose95ar1@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'jose95ar1@gmail.com', '0', '0000-00-00 00:00:00', '1', 2, 'jose1', 0, NULL, NULL, NULL, 'false'),
(3, 'jose95ar2@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'jose95ar2@gmail.com', '0', '0000-00-00 00:00:00', '1', 2, 'jose2', 0, NULL, NULL, NULL, 'false'),
(4, 'jose95ar3@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'jose95ar3@gmail.com', '0', '0000-00-00 00:00:00', '1', 3, 'jose3', 0, NULL, NULL, NULL, 'false'),
(5, 'jose95ar4@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'jose95ar4@gmail.com', '0', '0000-00-00 00:00:00', '1', 4, 'jose4', 0, NULL, NULL, NULL, 'false'),
(6, 'g@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'g@gmail.com', '0', '0000-00-00 00:00:00', '55', 4, 'Estefania', 0, NULL, NULL, NULL, 'false'),
(7, 'sergiosanchez1016@gmail.com', '3f402b2fadee4279d64efe095381f2f7', '', 'sergiosanchez1016@gmail.com', '0', '0000-00-00 00:00:00', '3013469743', 0, 'Sergio', 0, NULL, NULL, NULL, 'false'),
(8, 'jose@jose.com.ve', '25d55ad283aa400af464c76d713c07ad', '1579879002491.jpg', 'jose@jose.com.ve', '0', '0000-00-00 00:00:00', '4241668205', 2, 'jose artiles', 0, NULL, NULL, NULL, 'false'),
(9, 'danielmariscal69@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'danielmariscal69@gmail.com', '0', '0000-00-00 00:00:00', '5500112233', 4, 'Daniel ', 0, NULL, NULL, NULL, 'false'),
(10, 'davidvalor@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'davidvalor@gmail.com', '0', '0000-00-00 00:00:00', '04261187968', 4, 'davidvalor', 451, NULL, NULL, NULL, 'false'),
(11, 'angellus300@hotmail.com', '5e8667a439c68f5145dd2fcbecf02209', '', 'angellus300@hotmail.com', '0', '0000-00-00 00:00:00', '4411223366', 3, 'Daniel Mariscal', 0, '35.2271', '-80.8431', NULL, 'false'),
(12, 'conductor@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'conductor@gmail.com', '0', '0000-00-00 00:00:00', '4268056027', 2, 'Prueba Conductor', 0, '10.4944292', '-66.8277752', 'cT74xeHmyeU:APA91bENQ3-fY73tqcStRw6Ux09AEbpBgBzIxlviKygC-6AUU-TVWXImkU-M86EJsPz210zsu8J5bh6VBKvrBU6QdPCw3gfoWcEYrqI68nOUF_E9517afxO_Hk-bFdDQm2Ek7hl52Ijq', 'true'),
(13, 'cliente@gmail.com', '25d55ad283aa400af464c76d713c07ad', '', 'cliente@gmail.com', '0', '0000-00-00 00:00:00', '4268056027', 4, 'Prueba Cliente', 1002248261, '10.5009275', '-66.8201208', 'untokennuevo', 'false'),
(14, 'daniel@mail.com', 'b7a2c3b25c9441b0a38d0a874ace268b', '', 'daniel@mail.com', '0', '0158-10-12 00:29:20', '6633112244', 2, 'Daniel Mariscal', 0, '35.2284672', '-80.8436794', NULL, 'false'),
(15, 'luis@mail.com', '25d55ad283aa400af464c76d713c07ad', '', 'luis@mail.com', '0', '0000-00-00 00:00:00', '1122443366', 2, 'Luis Alfaro', 0, NULL, NULL, NULL, 'false'),
(16, 'carlos@mail.com', '5e8667a439c68f5145dd2fcbecf02209', '', 'carlos@mail.com', '0', '0000-00-00 00:00:00', '4411223344', 0, 'carlos', 0, NULL, NULL, NULL, 'false'),
(17, 'sergio@mail.com', '5e8667a439c68f5145dd2fcbecf02209', '', 'sergio@mail.com', '0', '0000-00-00 00:00:00', '3311664488', 2, 'sergio', 0, NULL, NULL, NULL, 'false'),
(18, 'tania@mail.com', '25d55ad283aa400af464c76d713c07ad', '', 'tania@mail.com', '0', '0000-00-00 00:00:00', '4455661133', 3, 'tania', 0, NULL, NULL, NULL, 'false');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `codes`
--
ALTER TABLE `codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `scheduled_turistrips`
--
ALTER TABLE `scheduled_turistrips`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `taxi_trips`
--
ALTER TABLE `taxi_trips`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wallet_history`
--
ALTER TABLE `wallet_history`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_photo`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `codes`
--
ALTER TABLE `codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT de la tabla `scheduled_turistrips`
--
ALTER TABLE `scheduled_turistrips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `taxi_trips`
--
ALTER TABLE `taxi_trips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT de la tabla `wallet_history`
--
ALTER TABLE `wallet_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
