import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActionSheetController, ToastController, LoadingController, AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service'
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  constructor(
    public form:FormBuilder, 
    public actionSheetController: ActionSheetController,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController,
    public storage: Storage,
    private authService: AuthenticationService,
    private _corek:CorekService,
    private router: Router
  ){ 

    this.loginForm = form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
      remember: [false]
    });

  }

  ngOnInit() {
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 4000
    });
    await toast.present();
  }

  async presentAlertConfirm(ID) {
    const alert = await this.alertController.create({
      header: 'No has registrado la documentación necesaria',
      message: 'No se ha completado el registro de documentos. Es necesario para ingresar en el sistema . ¿Deseas realizarlo ahora?',
      buttons: [
        {
          text: 'Si',
          handler: () => {
            this.router.navigate(['signin2'],ID);
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, 
      ]
    });

    await alert.present();
  }

  async login(){
    const loading = await this.loadingController.create({
      duration: 15000,
      message: 'Espere por favor...',
      translucent: true,
    });
    loading.present();
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
        let getLogin = Date.now()+'getLogin'+Math.random()
        this._corek.socket.emit('signon' ,{log:this.loginForm.value.email, pwd:this.loginForm.value.password,'event':getLogin});
        this._corek.socket.on(getLogin, (signon)=>{
          if(signon.token){
            loading.dismiss();
            switch(signon.user_status){
              case 0:
                this.presentAlertConfirm(signon.ID);
              break;
              case 1:
                this.presentToast('Aún no has sido aprobado');    
              break;
              case 2:
                localStorage.setItem('email', this.loginForm.value.email)
                localStorage.setItem('id', signon.ID)
                this.authService.login(signon.ID)
              break;
            }
          }else{
            loading.dismiss();
            this.presentToast('Los datos no concuerdan');
          }
        })
    });
  }

}
