import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActionSheetController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

  singinForm: FormGroup;

  constructor(public form:FormBuilder, 
    public actionSheetController: ActionSheetController,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController,
    private camera: Camera, 
    private _corek:CorekService,
    private router: Router,
    private transfer: FileTransfer
  ){
    
    this.singinForm = form.group({
      photo:  ['',],
      name: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.pattern("[a-zA-Z ]*")])],
      phone: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
      repassword: ['', Validators.required]
    }, {validator: this.passwordsMatch});

  }

  ngOnInit() {
  }

  passwordsMatch(cg: FormGroup){
    let pwd1 = cg.get('password').value;
    let pwd2 = cg.get('repassword').value;
    if ((pwd1 && pwd2) && pwd1 !== pwd2) {
      cg.controls['repassword'].setErrors({"pw_mismatch": true});
      return {"pw_mismatch": true};
    } else{
      return null;
    }
  }

  takePicture(sourceType){
    
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      this.presentToast('Espere mientras se sube el archivo');
      
      const namefile =  Date.now()+'.jpg' ; 
      let options1: FileUploadOptions = {
        fileKey: 'file',
        fileName: namefile,
        headers: {}
      }
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(imageData, 'http://157.230.191.152/upload.php', options1).then((data) => {
        this.singinForm.get('photo').setValue(namefile);
        this.presentToast('Se subio con exito.')
      }, (err) => {
        alert('Error. Intente de nuevo')
      });
    }, (err) => {
      alert('error');
    });
  }

  async getProfilePicture(){
    const actionSheet = await this.actionSheetController.create({
      header: 'Foto de pefil',
      buttons: [{
        text: 'Galeria',
        icon: 'md-archive',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY)
        }
      }, {
        text: 'Camara',
        icon: 'md-camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA)
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async signin(){
    const loading = await this.loadingController.create({
      duration: 15000,
      message: 'Espere por favor...',
      translucent: true,
    });
    loading.present();
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      let getUser = Date.now().toString() +'get_user'+Math.random();
      this._corek.socket.emit('get_users', {'condition':{'user_email':this.singinForm.value.email}, 'event':getUser}); 
      this._corek.socket.on(getUser, (users)=>{
        if(users.length>0){
          // Usuario ya registrado
          loading.dismiss();
          this.presentToast('El email ya se encuentra registrado.');
        }else{
          // Insertar usuario
          let insertUser = Date.now().toString()+"insertUser"+Math.random();
          this._corek.socket.emit('insert_user',{'insert':{
            'user_login':this.singinForm.value.email, 
            'user_pass':this.singinForm.value.password,
            'user_identification':0,
            'user_email':this.singinForm.value.email,
            'user_registered': Date.now(),
            'user_phone': this.singinForm.value.phone, 
            'user_status':0, 
            'user_photo':this.singinForm.value.photo,
            'display_name':this.singinForm.value.name,  
          }, 'event':insertUser});
          this._corek.socket.on(insertUser, (response)=>{
            loading.dismiss();
            this.presentToast('Registro Exitoso.');
            this.router.navigate(['signin2'],response.insertId );
          });
        }
      });
    });
  }

}
