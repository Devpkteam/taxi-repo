import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private popoverController: PopoverController
  ) { }

  ngOnInit() {}

  logout(){
    this.authService.logout();
  }

  close() {
    this.popoverController.dismiss();
  }
}
