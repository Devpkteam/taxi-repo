import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { FCM } from '@ionic-native/fcm/ngx';
import { AlertController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ToastController } from '@ionic/angular';
import { async } from '@angular/core/testing';
import { CorekService } from './services/corek.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authenticationService: AuthenticationService,
    private fcm:FCM,
    public alertController: AlertController,
    public toastController: ToastController,
    private _corek:CorekService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      let conection = Date.now().toString+'conection'+Math.random();
      this._corek.ConnectCorekconfig(conection).then((data)=>{
        this.authenticationService.authState.subscribe(state => {

          if (state) {
            switch (localStorage.getItem('notification-type')) {
              case 'cliente_viaje':     
                console.log('pase por la aute')
                localStorage.removeItem('notification-type')
                this.router.navigate(['/reload']).then(()=>{
                  this.router.navigate(['/detalle-taxiviaje']);
                });
                break;
            
              default:
                this.router.navigate(['tabs/tab1']);
  
                break;
            }
          } else {
            this.router.navigate(['']);
          }
        });
  
  
      })
      this.fcm.getToken().then(token => {
        localStorage.setItem('token',token);
  
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        localStorage.setItem('token',token);
      });

      
      

       this.fcm.onNotification().subscribe(async data => {
        console.log('pase por la notificacion')
        console.log(data);
        if(data.wasTapped){
          // push notification
          switch (data.type) {
            case 'cliente_viaje':
              localStorage.setItem('notification-type','cliente_viaje')
              localStorage.setItem('detalle', data.tripId)
              localStorage.setItem('type', 'taxi')
              this.router.navigate(['/detalle-taxiviaje']);
              break;
          
            default:
              break;
          }
        }else{
          // local notification
          if(data.type == "cliente_viaje"){
            const toast = await this.toastController.create({
            header: 'Nuevo viaje',
            message: 'Detectado un nuevo viaje cerca de usted',
            position: 'top',
            buttons: [
              {
                side: 'start',
                icon: 'person',
                text: 'Revisar',
                handler: () => {
                  localStorage.setItem('accion','taxi');
                  this.router.navigate(['tabs/tab1']);
                }
              }, {
                text: 'Ignorar',
                role: 'cancel',
                handler: () => {
                  console.log('Cancel clicked');
                }
              }
            ]
          });
          toast.present();
          }
          
       
        }
      });

     
    });
  }

 
}
