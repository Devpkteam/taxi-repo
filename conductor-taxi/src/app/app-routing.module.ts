import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./login/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',  
    loadChildren: () => import('./login/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signin',
    loadChildren: () => import('./login/signin/signin.module').then( m => m.SigninPageModule)
  },
  {
    path: 'signin2',
    loadChildren: () => import('./login/signin2/signin2.module').then( m => m.Signin2PageModule)
  },
  {
    path: 'signin3',
    loadChildren: () => import('./login/signin3/signin3.module').then( m => m.Signin3PageModule)
  },
  {
    path: 'restore-pass',
    loadChildren: () => import('./login/restore-pass/restore-pass.module').then( m => m.RestorePassPageModule)
  },
  {
    path: 'tabs',
    loadChildren: './pages/tabs/tabs.module#TabsPageModule',
    canActivate: [AuthGuardService]
  },
  {
    path: 'wallet',
    loadChildren: () => import('./pages/cartera/cartera.module').then( m => m.CarteraPageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./pages/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'detalle',
    loadChildren: () => import('./pages/detalles/detalles.module').then( m => m.DetallesPageModule)
  },
  {
    path: 'rating',
    loadChildren: () => import('./pages/rating/rating.module').then( m => m.RatingPageModule)
  },
  {
    path: 'detalle/turiviaje',
    loadChildren: () => import('./pages/detalle-turiviaje/detalle-turiviaje.module').then( m => m.DetalleTuriviajePageModule)
  },
  {
    path: 'detalle-taxiviaje',
    loadChildren: () => import('./pages/detalle-taxiviaje/detalle-taxiviaje.module').then( m => m.DetalleTaxiviajePageModule)
  },  {
    path: 'reload',
    loadChildren: () => import('./pages/reload/reload.module').then( m => m.ReloadPageModule)
  }


  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
