import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authState = new BehaviorSubject(false);

  constructor(
    private router: Router,
    private storage: Storage,
    private platform: Platform,
    public toastController: ToastController,
    private geolocation: Geolocation
  ){

    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });

  }

  ifLoggedIn() {
    this.storage.get('ID').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }

  getPos;


  login(ID) {
    this.storage.set('ID', ID).then((response) => {
      this.router.navigate(['tabs/tab1']);
      this.authState.next(true);
    });
  }
 
  logout() {
    this.storage.clear().then(() => {
      clearInterval(this.getPos);
      this.router.navigate(['login']);
      this.authState.next(false);
    });
  }
 
  isAuthenticated() {
    return this.authState.value;
  }

}
