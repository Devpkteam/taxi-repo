import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class ConductorService {

constructor(public http:HttpClient) { }

setTokenFCM(id,token){
  return this.http.post(`${URL_SERVICIOS}/usuario/fmctoken/${id}`,{token:token})
}

setConductorPosition(id,body){
  return this.http.post(`${URL_SERVICIOS}/usuario/conductor/position/${id}`,body)
}

sendAlertCliente(id){
  return this.http.post(`${URL_SERVICIOS}/usuario/alertar/cliente`,{id})
}

listenPosViaje(trip_id,user_login,pos_destino){
  return this.http.post(`${URL_SERVICIOS}/viaje/reportar`,{trip_id,user_login,pos_destino})
}
}
