import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage {
  client: string;
  fecha: string;

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    private router: Router,
  ) { 
    let type = localStorage.getItem('type');
    
    this.client = localStorage.getItem('client')
    this.fecha = localStorage.getItem('fecha')

  }

  calificar(){
    this.router.navigate(['tabs/tab1']);
  }

}
