import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

// GOOGLE MAPS API

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { ConductorService } from 'src/app/services/conductor.service';
import {  } from 'googlemaps';



@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage{

  DetalleID;
  opcion;
  trip;

  // VARIABLES DE LA RUTA
  locationStart;
  locationEnd;
  endPosition;

  // CODIGOS
  codInit; //ngModel
  codEnd; //ngModel
  codInitBD;
  codEndBD;

  // FUNCTION TO VALIDATE CODE INIT
  activarFinalizarViaje = false;
  activarInicioViaje = true;
  codInicioIncorrecto = false;
  codFinalIncorrecto = false;


  @ViewChild('map', {static: false}) mapElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map: any;
  address:string;
  whereIMlatLng;
  destination;
  accion = 'guia'
  trips: any[] = [];
  loader:boolean = true;
  geocoder = new google.maps.Geocoder;

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    public zone: NgZone,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private router: Router,
    public conductorService:ConductorService
  ) {
    
   }

  ionViewWillEnter() {
    console.log('entrando')
    this.DetalleID = localStorage.getItem('detalle');
    this.opcion = localStorage.getItem('type')
    console.log(this.DetalleID);
    console.log(this.opcion);
    
    if(this.opcion == 'taxi'){
      let taxiTripSql = 'SELECT * FROM taxi_trips WHERE id = '+this.DetalleID+' ;';
      let tripEvent = Date.now()+'scheduledTripsaa'+Math.random();
      this._corek.socket.emit('query',{'event':tripEvent, 'querystring':taxiTripSql});
      this._corek.socket.on(tripEvent, (responses)=>{ 
        this.trip = responses; 
        console.log(responses);

        this.locationStart = this.trip[0].startID;
        this.locationEnd = this.trip[0].endID;
        console.log(this.locationStart)
        console.log(this.locationEnd);
        this.loadMap();
        setTimeout(()=>{   
          this.calculateAndDisplayRoute();
        }, 100);
      });
    }else{
      let scheduledTripSql = 'SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID WHERE scheduled_turistrips.id = "'+this.DetalleID+'" ;';
      let tripEvent = Date.now()+'scheduledTripsaasa'+Math.random();
      this._corek.socket.emit('query',{'event':tripEvent, 'querystring':scheduledTripSql});
      this._corek.socket.on(tripEvent, (responses)=>{ 
        this.trip = responses; 
        let tripi = JSON.parse(this.trip[0].post_content);
        console.log(tripi[0].name);
        console.log(tripi[1].name);

        
        this.locationStart = tripi[0].name;
        this.locationEnd = tripi[1].name;
        this.loadMap();
        setTimeout(()=>{   
          this.calculateAndDisplayRoute();
        }, 100);
      });
    }


    // TRAE CODIGOS
    console.log(this.DetalleID)
    let codSql = 'SELECT * FROM codes WHERE trip_id = '+parseInt(this.DetalleID)+' ;';
    let codEvent = Date.now()+'traeme los codigos'+Math.random();
    this._corek.socket.emit('query',{'event':codEvent, 'querystring':codSql});
    this._corek.socket.on(codEvent, (responses)=>{ 
      console.log(responses);
      this.codInitBD = responses[0].cod_init;
      this.codEndBD = responses[0].cod_end;

      // VERIFICA SI ESTA COMENZADO EL VIAJE

      if(responses[0].dri_cod_init === 'true'){
        this.activarFinalizarViaje = true;
        this.activarInicioViaje = false;
        this.codInicioIncorrecto = false;
        this.codInit = responses[0].cod_init;
      }
    });
  }


  // FUNCTIONS OF GOOGLE MAPS API

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.whereIMlatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      
      
      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.directionsDisplay.setMap(this.map)
      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng,
        title     : 'Yo'
       });
      
 
      this.map.addListener('tilesloaded', () => {
        //'accuracy',this.map);
        
      });
 
    }).catch((error) => {
      //'Error getting location', error);
    });
  }
 
  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords "+lattitude+" "+longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log('entre')
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
        console.log('no address',this.address)
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
  }
  // FUNCTIONS OF GOOGLE MAPS API LISTO

  // FUNCTION CALC ROUTE

  calculateAndDisplayRoute() {
    this.geocoder.geocode({address :this.locationEnd},((results,status)=>{
      this.endPosition = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      }
    }))
    
    
    console.log('lol')
    const that = this;
    this.directionsService.route({
      origin: this.locationStart,
      destination: this.locationEnd,
      travelMode: google.maps.TravelMode.DRIVING
    }, (response, status) => {
      if (status === 'OK') {
        that.directionsDisplay.setDirections(response);
        console.log(response.routes[0].legs[0].distance)
        console.log(response.routes[0].legs[0].duration)
      } else {
        this.errorRoute();
      }
    });
  }

  // FUNCTION CALC ROUTE LISTO


  async errorRoute(){
    const alert = await this.alertCtrl.create({
      header: 'No se encontro una ruta valida.',
      subHeader: 'Ingrese otro lugar cercano.',
      buttons: [
        {
          text: 'Aceptar',
          role: 'agree',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  // FUNCTION TO VALIDATE CODE INIT

  viajeInicia(cod) {
    if(cod === this.codInitBD){
      this.activarFinalizarViaje = true;
      this.activarInicioViaje = false;
      this.codInicioIncorrecto = false;

      let inicioViajeSql = "UPDATE codes SET dri_cod_init = 'true' WHERE trip_id = '"+ localStorage.getItem('detalle') +"';"
      let inicioViajeEvent = Date.now()+'IniciaElViaje'+Math.random();
      this._corek.socket.emit('query',{'event':inicioViajeEvent, 'querystring':inicioViajeSql});
      this.conductorService.listenPosViaje(localStorage.getItem('detalle'),localStorage.getItem('email'),this.endPosition).subscribe();
    }else{
      console.log(cod)
      console.log(this.codInitBD)
      this.codInicioIncorrecto = true;
    }
  }

  viajeFinaliza(cod) {
    if(cod.toString() === this.codEndBD.toString()){
      this.router.navigate(['rating']);

      console.log(this.trip[0])
      localStorage.setItem('client', this.trip[0].client);
      localStorage.setItem('fecha', this.trip[0].time);

      let finalViajeSql = "UPDATE codes SET dri_cod_end = 'true' WHERE trip_id = '"+ localStorage.getItem('detalle') +"';"
      let finalViajeEvent = Date.now()+'IniciaElViaje'+Math.random();
      this._corek.socket.emit('query',{'event':finalViajeEvent, 'querystring':finalViajeSql});
      let deleteSql = `DELETE FROM taxi_trips WHERE id = ${this.DetalleID}`;
      let eliminaViaje = Date.now()+'EliminaElViaje'+Math.random();
      this._corek.socket.emit('query',{'event':eliminaViaje, 'querystring':deleteSql});
    }else{
      this.codFinalIncorrecto = true;
    }
  }

  
}
