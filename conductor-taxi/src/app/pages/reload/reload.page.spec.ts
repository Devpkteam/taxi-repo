import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReloadPage } from './reload.page';

describe('ReloadPage', () => {
  let component: ReloadPage;
  let fixture: ComponentFixture<ReloadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReloadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReloadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
