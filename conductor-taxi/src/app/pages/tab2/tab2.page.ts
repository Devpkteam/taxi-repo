import { Component, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  loader = true;
  trips = [];
  taxitrips=[];


  ionViewWillEnter(){
    this.loader = true;
    this.trips = [];
    this.taxitrips  = [];
    let tripSql = 'SELECT * FROM taxi_trips WHERE driver = "'+localStorage.getItem('email')+'" AND alert = "false";';
    let tripSql2 = 'SELECT * FROM scheduled_turistrips WHERE driver = "'+localStorage.getItem('email')+'" ;';
    let tripEvent = Date.now()+'scheduledTripsasda'+Math.random();
    let tripEvent2 = Date.now()+'scheduledTripsasda'+Math.random();
    this._corek.socket.emit('query',{'event':tripEvent, 'querystring':tripSql});
    this._corek.socket.emit('query',{'event':tripEvent2, 'querystring':tripSql2});
    this._corek.socket.on(tripEvent, (responses)=>{ 
      for(let x in responses){
        this.trips.push(responses[x])
      }
      console.log(this.trips);
      this.loader = false;
    });
    this._corek.socket.on(tripEvent2, (responses)=>{ 
      for(let x in responses){
        this.taxitrips.push(responses[x])
      }
      console.log(this.trips);
      this.loader = false;
    });
  }

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    public zone: NgZone,
    private router: Router,
  ){

  }

  GoToDetail(id: number, opcion: string){
    localStorage.setItem('detalle', id.toString())
    localStorage.setItem('type', opcion)
    this.router.navigate(['/detalle']);
  }
 
}
