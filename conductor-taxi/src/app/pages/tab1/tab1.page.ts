import { Component, ViewChild, ElementRef } from '@angular/core';
import { CorekService } from '../../services/corek.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

// GOOGLE MAPS COMPONENTS

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { ConductorService } from 'src/app/services/conductor.service';

declare var google;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  estiloViajeEnviado
  accion = 'guia'

  scheduledTrips = [];
  taxiTrips = [];

  // GOOGLE MAPS API

  @ViewChild('map', {static: false}) mapElement: ElementRef;
  map: any;
  address:string;

  message = 'No Disponible';
  trips: any[] = [];
  loader:boolean = true;

  constructor(
    private _corek:CorekService,
    private geolocation: Geolocation,
    public alertController: AlertController,
    private nativeGeocoder: NativeGeocoder,
    private router: Router,
    public conductorService:ConductorService
  ) {}




  ionViewWillEnter(){

    
    this.scheduledTrips = [];
    this.taxiTrips = [];
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.searchTrips();
    });
    this.loadMap();
    this.setTokenFCM();
    this.listenPos();
    this.loader = true;
    if(localStorage.getItem('accion') == 'taxi'){
      this.accion = 'taxi'
      localStorage.removeItem('accion')
    }
    

  }


  setTokenFCM(){
    this.conductorService.setTokenFCM(localStorage.getItem('id'),localStorage.getItem('token')).subscribe()
  }

  
  listenPos(){
    let watch = this.geolocation.watchPosition();
  watch.subscribe((data) => {
   // data can be a set of coordinates, or an error (if an error occurred).
   // data.coords.latitude
   // data.coords.longitude
   
    let body = {
      lat: data.coords.latitude,
      lng:data.coords.longitude
    }
   this.conductorService.setConductorPosition(localStorage.getItem('id'),body).subscribe()
  
  });
  }
  searchTrips(){
    let scheduledTripSql = 'SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID WHERE scheduled_turistrips.driver is NULL  ;';
    let taxiTripSql2 = 'SELECT * FROM taxi_trips WHERE driver is NULL AND alert = "false" ;';
    let scheduledTripEvent = Date.now()+'scheduledTripsaa'+Math.random();
    let taxiTripEvent = Date.now()+'scheduledTripsaa'+Math.random();
    this._corek.socket.emit('query',{'event':scheduledTripEvent, 'querystring':scheduledTripSql});
    console.log("enviado")
    this._corek.socket.on(scheduledTripEvent, (responses)=>{ 
      console.log(responses)
      for(let x in responses){
      
        let parse = JSON.parse(responses[x].post_content);
        
        responses[x].post_content = parse;
      }
      this.scheduledTrips = responses; 
   
      this.loader = false;
    });
    this._corek.socket.emit('query',{'event':taxiTripEvent, 'querystring':taxiTripSql2});
    console.log("enviado")
    this._corek.socket.on(taxiTripEvent, (responses)=>{ 
      for(let x in responses){
        this.taxiTrips.push(responses[x])
      }
  
      this.loader = false;
    });
  }

  reserveTrip(ID){
    let updatePTrips = Date.now().toString+'updatePost'+Math.random();
    this._corek.socket.emit("update_post",{"set":{'post_status':1},"condition":{"ID":ID}, 'event':updatePTrips});
    this._corek.socket.on(updatePTrips, (response) => {
      this.loader = true;
      this.searchTrips();
    });

  }

  changeToggle(event){
    this.message = (event.detail.checked) ? 'Disponible': 'No Disponible'   
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
 
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng
       });
 
      this.map.addListener('tilesloaded', () => {
        console.log('accuracy',this.map);
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
      });
 
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
 
  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords "+lattitude+" "+longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
 
  }

  async takeTrip(id: number, type: string) {
    //type: turis o taxi
    const alert = await this.alertController.create({
      header: '¿Esta seguro que quiere aceptar este viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Confirmar',
          role: 'ok',
          handler: () => {
            this.estiloViajeEnviado  = 'estiloDesactivado';
            this.conductorService.sendAlertCliente(id).subscribe();
            if(type == 'taxi'){
              let scheduleTripSql = 'UPDATE taxi_trips SET driver = "'+ localStorage.getItem('email') +'" WHERE id = '+id+'';
              let scheduleTripEvent = Date.now()+'taxi_trip'+Math.random();
              this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
              this._corek.socket.on(scheduleTripEvent, (responses)=>{this.estiloViajeEnviado  = '';console.log(responses); this.router.navigate(['/tabs/tab2']);});
            }else{
              let scheduleTripSql = 'UPDATE scheduled_turistrips SET driver = "'+ localStorage.getItem('email') +'" WHERE id = '+id+'';
              let scheduleTripEvent = Date.now()+'taxi_tripa'+Math.random();
              this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
              this._corek.socket.on(scheduleTripEvent, (responses)=>{this.estiloViajeEnviado  = '';console.log(responses); this.router.navigate(['/tabs/tab2']);});
            }
          }
        },
        {
          text: 'Calcelar',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }

  GoToDetail(id: number, opcion: string){
    localStorage.setItem('detalle', id.toString())
    localStorage.setItem('type', opcion)
    this.router.navigate(['/detalle-taxiviaje']);
  }
}
