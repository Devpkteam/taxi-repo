import { Component, OnInit, Input } from '@angular/core';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  public img:any;
  public title:String;
  public message:String;

  constructor(navParams: NavParams) {
    this.img = navParams.get('image');
    this.title = navParams.get('title');
    this.message = navParams.get('content');
  }

  ngOnInit() {
  }

}
