import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTuriviajePageRoutingModule } from './detalle-turiviaje-routing.module';

import { DetalleTuriviajePage } from './detalle-turiviaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTuriviajePageRoutingModule
  ],
  declarations: [DetalleTuriviajePage]
})
export class DetalleTuriviajePageModule {}
