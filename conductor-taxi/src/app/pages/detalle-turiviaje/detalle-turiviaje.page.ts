import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

// GOOGLE MAPS API

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { } from 'googlemaps';

@Component({
  selector: 'app-detalle-turiviaje',
  templateUrl: './detalle-turiviaje.page.html',
  styleUrls: ['./detalle-turiviaje.page.scss'],
})
export class DetalleTuriviajePage {

  DetalleID;
  opcion;
  trip;

  codStart;
  codEnd;

  // VARIABLES DE LA RUTA
  locationStart;
  locationEnd;

  @ViewChild('map', {static: false}) mapElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map: any;
  address:string;
  whereIMlatLng;
  destination;
  accion = 'guia'
  trips: any[] = [];
  loader:boolean = true;

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    public zone: NgZone,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private router: Router
  ) {
    
   }

  ionViewWillEnter() {
    this.DetalleID = localStorage.getItem('detalle-turiviaje');
    this.opcion = localStorage.getItem('type')
    console.log(this.DetalleID);
    console.log(this.opcion);
    
    if(this.opcion == 'taxi'){
      let taxiTripSql = 'SELECT * FROM taxi_trips WHERE id = '+this.DetalleID+' ;';
      let tripEvent = Date.now()+'scheduledTripsaa'+Math.random();
      this._corek.socket.emit('query',{'event':tripEvent, 'querystring':taxiTripSql});
      this._corek.socket.on(tripEvent, (responses)=>{ 
        this.trip = responses; 
        this.locationStart = this.trip[0].startID;
        this.locationEnd = this.trip[0].endID;
        let codSql = 'SELECT * FROM codes WHERE trip_id = '+this.DetalleID+' AND trip_type = "taxi";';
        let codEvent = Date.now()+'scheduledTripsaa'+Math.random();
        this._corek.socket.emit('query',{'event':codEvent, 'querystring':codSql});
        this._corek.socket.on(codEvent, (responses)=>{
          console.log(responses[0].cod_init)
          this.codStart = responses[0].cod_init;
          this.codEnd = responses[0].cod_end;
        });
        this.loadMap();
        setTimeout(()=>{   
          this.calculateAndDisplayRoute();
        }, 100);
      });
    }else{
      let scheduledTripSql = 'SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID WHERE scheduled_turistrips.id = "'+this.DetalleID+'" ;';
      let tripEvent = Date.now()+'scheduledTripsaasa'+Math.random();
      this._corek.socket.emit('query',{'event':tripEvent, 'querystring':scheduledTripSql});
      this._corek.socket.on(tripEvent, (responses)=>{ 
        this.trip = responses; 
        let tripi = JSON.parse(this.trip[0].post_content);
        console.log(tripi[0].name);
        console.log(tripi[1].name);
        this.locationStart = tripi[0].name;
        this.locationEnd = tripi[1].name;
        let codSql = 'SELECT * FROM codes WHERE trip_id = '+this.DetalleID+' AND trip_type = "turis";';
        let codEvent = Date.now()+'scheduledTripsaa'+Math.random();
        this._corek.socket.emit('query',{'event':codEvent, 'querystring':codSql});
        this._corek.socket.on(codEvent, (responses)=>{
          console.log(responses[0].cod_init)
          this.codStart = responses[0].cod_init;
          this.codEnd = responses[0].cod_end;
        });
        this.loadMap();
        setTimeout(()=>{   
          this.calculateAndDisplayRoute();
        }, 100);
      });
    }
  }


  // FUNCTIONS OF GOOGLE MAPS API

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.whereIMlatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      
      
      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.directionsDisplay.setMap(this.map)
      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng,
        title     : 'Yo'
       });
      
 
      this.map.addListener('tilesloaded', () => {
        //'accuracy',this.map);
        
      });
 
    }).catch((error) => {
      //'Error getting location', error);
    });
  }
 
  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords "+lattitude+" "+longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log('entre')
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
        console.log('no address',this.address)
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
  }
  // FUNCTIONS OF GOOGLE MAPS API LISTO

  // FUNCTION CALC ROUTE

  calculateAndDisplayRoute() {
    let mode =  google.maps.TravelMode.DRIVING
    console.log('lol')
    const that = this;
    this.directionsService.route({
      origin: this.locationStart,
      destination: this.locationEnd,
      travelMode: google.maps.TravelMode.DRIVING
    }, (response, status) => {
      if (status === 'OK') {
        that.directionsDisplay.setDirections(response);
        console.log(response.routes[0].legs[0].distance)
        console.log(response.routes[0].legs[0].duration)
      } else {
        this.errorRoute();
      }
    });
  }

  // FUNCTION CALC ROUTE LISTO


  async errorRoute(){
    const alert = await this.alertCtrl.create({
      header: 'No se encontro una ruta valida.',
      subHeader: 'Ingrese otro lugar cercano.',
      buttons: [
        {
          text: 'Aceptar',
          role: 'agree',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  async reportTrip(){
    const alert = await this.alertCtrl.create({
      header: 'Seguro que desea reportar este viaje?',
      buttons: [
        {
          text: 'Si',
          role: 'agree',
          handler: () => {
            this.exito()
          }
        },
        {
          text: 'No',
        role: 'disagree',
        handler: () => {
        }
      }
      ]
    });

    await alert.present();
  }

  async exito(){
    const alert = await this.alertCtrl.create({
      header: 'Su Viaje a sido  reportado exitosamente',
      buttons: [{
        text:'Aceptar',
        role:'acept',
        handler: ()=>{
          console.log(this.trip[0].driver)
         
                   
          let sql = `UPDATE taxi_trips SET alert = "true" WHERE id = ${this.trip[0].id};`;

         

          let report = Date.now().toString()+"Reportando Viaje"+Math.random();
          this._corek.socket.emit('query', {'event':report, 'querystring':sql});
          this._corek.socket.on(report, (res) =>{
            console.log(res)
          })
          this.router.navigate(['tabs/tab1']);
        }
      }]
    })
    await alert.present();
  }
  
}
