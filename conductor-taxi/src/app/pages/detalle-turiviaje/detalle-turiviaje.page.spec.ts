import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetalleTuriviajePage } from './detalle-turiviaje.page';

describe('DetalleTuriviajePage', () => {
  let component: DetalleTuriviajePage;
  let fixture: ComponentFixture<DetalleTuriviajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTuriviajePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetalleTuriviajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
