import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleTuriviajePage } from './detalle-turiviaje.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleTuriviajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleTuriviajePageRoutingModule {}
