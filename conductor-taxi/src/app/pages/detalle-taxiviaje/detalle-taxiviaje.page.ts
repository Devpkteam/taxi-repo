import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

// GOOGLE MAPS API

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { ConductorService } from 'src/app/services/conductor.service';
import {  } from 'googlemaps';



@Component({
  selector: 'app-detalle-taxiviaje',
  templateUrl: './detalle-taxiviaje.page.html',
  styleUrls: ['./detalle-taxiviaje.page.scss'],
})
export class DetalleTaxiviajePage  {
  DetalleID;
  opcion;
  trip;

  // VARIABLES DE LA RUTA
  locationStart;
  locationEnd;
  endPosition;
  ;
  // CODIGOS
  codInit; //ngModel
  codEnd; //ngModel
  codInitBD;
  codEndBD;

  // FUNCTION TO VALIDATE CODE INIT
  activarFinalizarViaje = false;
  activarInicioViaje = true;
  codInicioIncorrecto = false;
  codFinalIncorrecto = false;
  estiloViajeEnviado;

  @ViewChild('map', {static: false}) mapElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map: any;
  address:string;
  whereIMlatLng;
  destination;
  accion = 'guia'
  trips: any[] = [];
  loader:boolean = true;
  geocoder = new google.maps.Geocoder;

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    public zone: NgZone,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private router: Router,
    public conductorService:ConductorService,
    public alertController:AlertController
  ) {
    
   }

  ionViewWillEnter() {
    
    this.DetalleID = localStorage.getItem('detalle');
    this.opcion = localStorage.getItem('type')
    console.log('llegue al detalle')
    
   console.log(this.DetalleID,this.opcion);
    
    if(this.opcion == 'taxi'){
      this.typeTaxi()
    }else{
      this.typeOther()
    }


    // TRAE CODIGOS

  }


  typeTaxi(){
    let taxiTripSql = 'SELECT * FROM taxi_trips WHERE id = '+this.DetalleID+' ;';
    let tripEvent = Date.now()+'scheduledTripsaa'+Math.random();
    this._corek.socket.emit('query',{'event':tripEvent, 'querystring':taxiTripSql});
    this._corek.socket.on(tripEvent, (responses)=>{ 
      this.trip = responses; 
      

      this.locationStart = this.trip[0].startID;
      this.locationEnd = this.trip[0].endID;
      
      
      this.loadMap();
      setTimeout(()=>{   
        this.calculateAndDisplayRoute();
      }, 100);
    });
  }

  typeOther(){
    let scheduledTripSql = 'SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID WHERE scheduled_turistrips.id = "'+this.DetalleID+'" ;';
    let tripEvent = Date.now()+'scheduledTripsaasa'+Math.random();
    this._corek.socket.emit('query',{'event':tripEvent, 'querystring':scheduledTripSql});
    this._corek.socket.on(tripEvent, (responses)=>{ 
      this.trip = responses; 
      let tripi = JSON.parse(this.trip[0].post_content);
      
      

      
      this.locationStart = tripi[0].name;
      this.locationEnd = tripi[1].name;
      this.loadMap();
      setTimeout(()=>{   
        this.calculateAndDisplayRoute();
      }, 100);
    });
  }

  // FUNCTIONS OF GOOGLE MAPS API

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.whereIMlatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      
      
      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.directionsDisplay.setMap(this.map)
      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng,
        title     : 'Yo'
       });
      
 
      this.map.addListener('tilesloaded', () => {
        //'accuracy',this.map);
        
      });
 
    }).catch((error) => {
      //'Error getting location', error);
    });
  }
 
  getAddressFromCoords(lattitude, longitude) {
    
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
        
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
  }
  // FUNCTIONS OF GOOGLE MAPS API LISTO

  // FUNCTION CALC ROUTE

  goBack(){
    this.router.navigate(['/tabs/tab1'])
  }

  calculateAndDisplayRoute() {
    this.geocoder.geocode({address :this.locationEnd},((results,status)=>{
      this.endPosition = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      }
    }))
    

    
    
    const that = this;
    this.directionsService.route({
      origin: this.locationStart,
      destination: this.locationEnd,
      travelMode: google.maps.TravelMode.DRIVING
    }, (response, status) => {
      if (status === 'OK') {
        that.directionsDisplay.setDirections(response);
        
        
      } else {
        this.errorRoute();
      }
    });
  }

  // FUNCTION CALC ROUTE LISTO


  async errorRoute(){
    const alert = await this.alertCtrl.create({
      header: 'No se encontro una ruta valida.',
      subHeader: 'Ingrese otro lugar cercano.',
      buttons: [
        {
          text: 'Aceptar',
          role: 'agree',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  // FUNCTION TO VALIDATE CODE INIT
  async takeTrip() {
    let id = this.DetalleID;
    let type = this.opcion
    //type: turis o taxi
    const alert = await this.alertController.create({
      header: '¿Esta seguro que quiere aceptar este viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Confirmar',
          role: 'ok',
          handler: () => {
            this.estiloViajeEnviado  = 'estiloDesactivado';
            this.conductorService.sendAlertCliente(id).subscribe();
            if(type == 'taxi'){
              let scheduleTripSql = 'UPDATE taxi_trips SET driver = "'+ localStorage.getItem('email') +'" WHERE id = '+id+'';
              let scheduleTripEvent = Date.now()+'taxi_trip'+Math.random();
              this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
              this._corek.socket.on(scheduleTripEvent, (responses)=>{this.estiloViajeEnviado  = ''; this.router.navigate(['/tabs/tab2']);});
            }else{
              let scheduleTripSql = 'UPDATE scheduled_turistrips SET driver = "'+ localStorage.getItem('email') +'" WHERE id = '+id+'';
              let scheduleTripEvent = Date.now()+'taxi_tripa'+Math.random();
              this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
              this._corek.socket.on(scheduleTripEvent, (responses)=>{this.estiloViajeEnviado  = ''; this.router.navigate(['/tabs/tab2']);});
            }
          }
        },
        {
          text: 'Calcelar',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }

}
