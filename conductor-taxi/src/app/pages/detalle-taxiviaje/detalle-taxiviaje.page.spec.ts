import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetalleTaxiviajePage } from './detalle-taxiviaje.page';

describe('DetalleTaxiviajePage', () => {
  let component: DetalleTaxiviajePage;
  let fixture: ComponentFixture<DetalleTaxiviajePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTaxiviajePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetalleTaxiviajePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
