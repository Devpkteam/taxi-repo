import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleTaxiviajePageRoutingModule } from './detalle-taxiviaje-routing.module';

import { DetalleTaxiviajePage } from './detalle-taxiviaje.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleTaxiviajePageRoutingModule
  ],
  declarations: [DetalleTaxiviajePage]
})
export class DetalleTaxiviajePageModule {}
