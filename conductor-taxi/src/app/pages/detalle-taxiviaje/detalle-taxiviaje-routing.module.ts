import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalleTaxiviajePage } from './detalle-taxiviaje.page';

const routes: Routes = [
  {
    path: '',
    component: DetalleTaxiviajePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalleTaxiviajePageRoutingModule {}
