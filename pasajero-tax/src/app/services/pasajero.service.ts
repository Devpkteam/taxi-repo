import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_SERVICIOS } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class PasajeroService {

constructor(public http:HttpClient) { }


setPasajeroPosition(id,body){
  console.log('mandando posicion')
  return this.http.put(`${URL_SERVICIOS}/usuario/pasajero/position/${id}`,body)
}

getDistancia(){
  return this.http.post(`${URL_SERVICIOS}/usuario/distancia`,{lat1:10.5009275,lng1:-66.8201208,lat2:12.4919473,lng2:-66.86306})
}

setPasajeroTokenFCM(id,token){
  return this.http.post(`${URL_SERVICIOS}/usuario/fmctoken/${id}`,{token:token})
}

alertToDriversNear(startPosition,idTrip){
    return this.http.post(`${URL_SERVICIOS}/viaje/alert`,{startPosition,idTrip})
    
}

reportDriver(user_login){
  return this.http.post(`${URL_SERVICIOS}/usuario/reportar/conductor`,{user_login})
}

}
