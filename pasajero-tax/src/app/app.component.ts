import { Component } from '@angular/core';

import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private authenticationService: AuthenticationService,
    private fcm: FCM,
    public toastController: ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.fcm.getToken().then(token => {
        localStorage.setItem('token',token);
  
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        localStorage.setItem('token',token);
      });
      this.authenticationService.authState.subscribe(state => {
        if (state) {
          switch (localStorage.getItem('notification-type')) {
            case 'viaje_aceptado':
                  localStorage.removeItem('notification-type')
                  this.router.navigate(['/reload']).then(()=>{
                    this.router.navigate(['/detalle'])
                  })
              break;
          
            default:
              this.router.navigate(['tabs/tab1']);
              break;
          }
         
        } else {
          this.router.navigate(['']);
        }
      });
    });

    
    this.fcm.onNotification().subscribe(async data => {
      navigator.vibrate([1000, 1000, 1000, 1000]);
      console.log(data);
      if(data.wasTapped){
        switch (data.type) {
          case 'viaje_aceptado':
            console.log('viaje aceptado')
            localStorage.setItem('detalle', data.trip_id)
            localStorage.setItem('type', 'taxi')
            localStorage.setItem('notification-type','viaje_aceptado')
            this.router.navigate(['/detalle']);
            break;
        
          default:
            break;
        }

      }else{
        // local notification
        if(data.type == 'viaje_aceptado'){
          const toast = await this.toastController.create({
          header: 'Viaje aceptado',
          message: 'Un conductor ha aceptado su solicitud de viaje',
          position: 'top',
          buttons: [
            {
              side: 'start',
              icon: 'car',
              text: 'Mas información',
              handler: () => {
                
                localStorage.setItem('taxitrip_id',data.trip_id);
                localStorage.setItem('type', 'taxi')
                localStorage.setItem('trip_accept','true');
                this.router.navigate(['/detalle']);
              }
            }
          ]
        });
        toast.present();
        }
        
      }
    });
  }
}
