import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer} from '@ionic-native/file-transfer/ngx';
import { CorekService } from './services/corek.service'
import { IonicStorageModule } from '@ionic/storage';
import { AuthenticationService } from './services/authentication.service'
import { AuthGuardService } from './services/auth-guard.service'
import { File } from '@ionic-native/file/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { HttpClientModule } from '@angular/common/http';

import { DatePicker } from '@ionic-native/date-picker/ngx';
// API GOOGLE
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { ComponentsModule } from './components/components.module';
import { PopoverComponent } from './components/popover/popover.component';
import { FCM } from '@ionic-native/fcm/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    PopoverComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule, 
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    ComponentsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    CorekService,
    AuthGuardService,
    AuthenticationService,
    File,
    FilePath,
    FileTransfer,
    Geolocation,
    NativeGeocoder,
    FCM
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
