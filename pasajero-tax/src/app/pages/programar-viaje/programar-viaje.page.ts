import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service'

import { DatePicker } from '@ionic-native/date-picker/ngx';

// GOOGLE MAPS API

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Router } from '@angular/router';

import * as moment from 'moment';

declare var google;

@Component({
  selector: 'app-programar-viaje',
  templateUrl: './programar-viaje.page.html',
  styleUrls: ['./programar-viaje.page.scss'],
})
export class ProgramarViajePage implements OnInit {

  estiloViajeEnviado;

  // DESHABILITAR BOTONES

  desacReport = false;

  // VARIABLES TURISVIAJE BASE DE DATOS

  languaje;
  trip_date;
  amount = 0;
  pos_start;
  pos_finished;
  turisviaje_id;


  /*****************************/
  choice = false;

  // Day Picker
  today;
  day;

  // COUNTER

  timer_days = 0;
  timer_hours = 0;
  timer_minutes = 0;
  timer_secs= 0;


  // MAP: API GOOGLE

  @ViewChild('map', {static: false}) mapElement: ElementRef;
  map: any;
  address:string;

  constructor(
    public alertController: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private datePicker: DatePicker,
    private _corek:CorekService,
    private router: Router
  ) { 
    let choice = localStorage.getItem('tab1-touch');
    this.turisviaje_id = localStorage.getItem('tab1-turisviaje-id');
    if(choice === 'take'){
      this.day = new Date();
      this.choice = true;
    }
   }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.loadMap();
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Seleccione el idioma de su conductor',
      subHeader: '',
      inputs: [
        {
          name: 'idioma',
          type: 'radio',
          label: 'Español',
          value: 'spanish',
          checked: true
        },
        {
          name: 'idioma',
          type: 'radio',
          label: 'Ingles',
          value: 'english',
          checked: false
        },
      ],
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: (input) => {
            this.languaje = input;
          }
        }
      ]
    });

    await alert.present();
  }

  // FUNCTIONS OF GOOGLE MAPS API

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      
      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      
 
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng
       });
 
      this.map.addListener('tilesloaded', () => {
        //'accuracy',this.map);
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
      });
 
    }).catch((error) => {
      //'Error getting location', error);
    });
  }
 
  getAddressFromCoords(lattitude, longitude) {
    //"getAddressFromCoords "+lattitude+" "+longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
  }

  // DATE PICKER

  showDatePicker(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => console.log('Got date: ', date),
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  // COUNT DOWN

  dayChange(day){
    var ahora = false;
    day == undefined ? console.log('falto uno') : ahora=true

    if(ahora){
      var date = new Date(day).getTime();
        var now = new Date().getTime(); 
        var t = date - now; 
        this.timer_days = Math.floor(t / (1000 * 60 * 60 * 24)); 
        this.timer_hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
        this.timer_minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
        this.timer_secs = Math.floor((t % (1000 * 60)) / 1000);
        if(t<0){
          this.timer_days = 0;
          this.timer_hours =0;
          this.timer_minutes = 0;
          this.timer_secs =0;
        }
        setTimeout(()=>{
          this.dayChange(this.day);
        }, 1000);
    }

  }

  async cancelar() {
    const alert = await this.alertController.create({
      header: '¿Estas seguro de cancelar su viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Si',
          role: 'ok',
          handler: (resp) => {
            this.router.navigate(['/tabs']);
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }


  async pago() {
    const alert = await this.alertController.create({
      header: '¿Estas seguro de aceptar este viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Realizar Pago',
          role: 'ok',
          handler: (resp) => {
            let codStart: number  = Math.random() * 100000;
            let codEnd: number = Math.random() * 100000;
            codStart = parseInt(codStart.toFixed());
            codEnd = parseInt(codEnd.toFixed());
            this.estiloViajeEnviado  = 'estiloDesactivado';
            /*let scheduleTripSql = 'INSERT INTO scheduled_turistrips (client, time)';
                scheduleTripSql += 'VALUES ("'+ localStorage.getItem('email') +'", "'+ day +'");';
            
            let walletHistorySql = 'INSERT INTO wallet_history (user, monto, tipo, fecha) VALUES ("'+ localStorage.getItem('email') +'", "'+ this.amount +'", "'+ 'Viaje Turistico' +'", "'+ Date.now() +'");';

            let scheduleTripEvent = Date.now()+'turisviaje'+Math.random();
            let cobrarSqlEvent = Date.now()+'turisviaje'+Math.random();
            let walletHistoryEvent = Date.now()+'turisviaje'+Math.random();

            this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
            this._corek.socket.emit('query',{'event':cobrarSqlEvent, 'querystring':cobrarSql});
            this._corek.socket.emit('query',{'event':walletHistoryEvent, 'querystring':walletHistorySql});

            this._corek.socket.on(scheduleTripEvent, (responses)=>{ console.log(responses)});
            this._corek.socket.on(cobrarSqlEvent, (responses)=>{ console.log(responses);});
            this._corek.socket.on(walletHistorySql, (responses)=>{ console.log(responses);});*/

            let day;
            this.choice === true ? day = new Date(this.day) : day = new Date(this.day);
            var date = new Date(day).getTime();
            var now = new Date().getTime(); 
            var t = date - now;
            if(this.day !== undefined){
              if(t>0 || this.choice == true){
                // Mandar a corek, viaje programado
                let walletSql = 'SELECT cash FROM wp_users WHERE user_login = "'+localStorage.getItem('email')+'";';
                let walletEvent = Date.now()+'turisviaje'+Math.random();
                this._corek.socket.emit('query',{'event':walletEvent, 'querystring':walletSql});
                this._corek.socket.on(walletEvent, (responses)=>{ 
                  console.log(responses)
                  if(this.amount > responses[0].cash){
                    this.pleaseRecharge();
                  }else{
                    let cobrarSql = "UPDATE wp_users SET cash = (cash - "+ this.amount +") WHERE user_login = '"+ localStorage.getItem('email') +"';";
                    let cobrarSqlEvent = Date.now()+'turisviaje'+Math.random();
                    this._corek.socket.emit('query',{'event':cobrarSqlEvent, 'querystring':cobrarSql});
                    this._corek.socket.on(cobrarSqlEvent, (responses)=>{ 
                      console.log(responses)
                      let scheduleTripSql = 'INSERT INTO scheduled_turistrips (viaje_id,client, time, precio)';
                          scheduleTripSql += 'VALUES ("'+ this.turisviaje_id +'","'+ localStorage.getItem('email') +'", "'+ day +'", "'+ this.amount +'");';
                      let scheduleTripEvent = Date.now()+'turisviaje'+Math.random();
                      this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
                      this._corek.socket.on(scheduleTripEvent, (responses)=>{ 
                        console.log(responses)
                        let tripId = responses.insertId;
                        let walletHistorySql = 'INSERT INTO wallet_history (user, monto, tipo, fecha) VALUES ("'+ localStorage.getItem('email') +'", "'+ this.amount +'", "'+ 'Viaje Turistico' +'", "'+ Date.now() +'");';
                        let walletHistoryEvent = Date.now()+'turisviaje'+Math.random();
                        this._corek.socket.emit('query',{'event':walletHistoryEvent, 'querystring':walletHistorySql});
                        this._corek.socket.on(walletHistoryEvent, (responses)=>{ 
                          console.log(responses);
                          let codSql = 'INSERT INTO codes (trip_id, trip_type, cod_init, cod_end, dri_cod_init, dri_cod_end)'; 
                          codSql += 'VALUES ("'+ tripId +'","'+ 'turis' +'","'+ codStart +'","'+ codEnd +'","'+ false +'","'+ false +'");';
                          let codEvent = Date.now()+'taxi_trip'+Math.random();
                          this._corek.socket.emit('query',{'event':codEvent, 'querystring':codSql});
                          this._corek.socket.on(codEvent, (responses)=>{ 
                            console.log(responses);

                            this.router.navigate(['/tabs/tab2']);
                          });
                        });
                      });
                    });
                  }
                });
              }else{
              }  
            }else{
              this.alertDay();
            }
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

  async reportTrip(){
    const alert = await this.alertController.create({
      header: '¿Deseas reportar este viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Si',
          role: 'agree',
          handler: () => {
            this.desacReport = true;
          }
        },
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

  async alertDay(){
    const alert = await this.alertController.create({
      header: 'Porfavor selecciona una fecha y una hora valida.',
      subHeader: '',
    });

    await alert.present();
  }

  async pleaseRecharge(){
    const alert = await this.alertController.create({
      header: 'Porfavor recargue para poder solicitar su viaje.',
      subHeader: '',
    });

    await alert.present();
  }

}
