import { Component, NgZone } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  // GOOGLE AUTOCOMPLETE

  GoogleAutocomplete: google.maps.places.AutocompleteService;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;

  // GOOGLE AUTOCOMPLETE FINISHED

  loader = true;
  scheduledTrips;
  taxiTrips = [];

  ionViewWillEnter(){
    this.scheduledTrips = [];
    this.taxiTrips = [];
    //SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID;
    let scheduledTripSql = 'SELECT * FROM scheduled_turistrips INNER JOIN wp_posts ON scheduled_turistrips.viaje_id=wp_posts.ID WHERE scheduled_turistrips.client = "'+localStorage.getItem('email')+'" ;';
    let taxiTripSql2 = `SELECT * FROM taxi_trips WHERE client = "${localStorage.getItem('email')}" AND  alert = 'false';`;
    let scheduledTripEvent = Date.now()+'scheduledTripsaa'+Math.random();
    let taxiTripEvent = Date.now()+'scheduledTripsaa'+Math.random();
    this._corek.socket.emit('query',{'event':scheduledTripEvent, 'querystring':scheduledTripSql});
    console.log("enviado")
    this._corek.socket.on(scheduledTripEvent, (responses)=>{ 
      this.scheduledTrips = responses; 
      console.log(responses);
      this.loader = false;
    });
    this._corek.socket.emit('query',{'event':taxiTripEvent, 'querystring':taxiTripSql2});
    console.log("enviado")
    this._corek.socket.on(taxiTripEvent, (responses)=>{ 
      for(let x in responses){
        this.taxiTrips.push(responses[x])
      }
      console.log(responses);
      this.loader = false;
    });

    if(localStorage.getItem('trip_accept') == 'true'){
      let id = localStorage.getItem('taxitrip_id');
      localStorage.removeItem('trip_accept')
      localStorage.removeItem('taxitrip_id')
      this.GoToDetail(Number(id),'taxi')
    }
  }

  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService,
    public zone: NgZone,
    private router: Router,
  ){
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
  }

  // GOOGLE AUTOCOMPLETE

  updateSearchResults(){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item) {
    console.log(item)
    this.location = item
    this.placeid = this.location.place_id
    console.log('placeid'+ this.placeid)
  }
  GoTo(){
    return window.location.href = 'https://www.google.com/maps/place/?q=place_id:'+this.placeid;
  }

  

  // GOOGLE AUTOCOMPLETE FINISHED

  GoToDetail(id: number, opcion: string){
    localStorage.setItem('detalle', id.toString())
    localStorage.setItem('type', opcion)
    this.router.navigate(['/detalle']);
  }

}