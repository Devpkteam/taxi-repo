
import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { CorekService } from '../../services/corek.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

// GOOGLE MAPS API

import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { PasajeroService } from 'src/app/services/pasajero.service';

import {} from 'googlemaps';




@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  whereIAM;
  languaje;
  precio;
  estiloViajeEnviado;
  pedir_viaje = false;

  // GOOGLE AUTOCOMPLETE

  GoogleAutocomplete: google.maps.places.AutocompleteService;
  autocompleteStart: { input: string; };
  autocomplete: { input: string; };
  autocompleteItems: any[];
  autocompleteItemsStart: any[];
  location: any;
  placeid: any;
  locationStart: any;
  placeidStart: any; 


  // GOOGLE AUTOCOMPLETE FINISHED



  googleAutocomplete;

  // MAP: API GOOGLE

  @ViewChild('map', {static: false}) mapElement: ElementRef;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  map: any;
  address:string;
  whereIMlatLng;
  destination;
  accion = 'guia'
  trips: any[] = [];
  loader:boolean = true;
  constructor(
    private _corek:CorekService,
    public alertController: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    private router: Router,
    public zone: NgZone,
    public pasajeroService:PasajeroService
  ) {
    
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.autocompleteStart = { input: '' };
    this.autocompleteItemsStart = [];
    this.loadMap();
    
  }
  geocoder = new google.maps.Geocoder;
  
  ionViewWillEnter(){
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.searchTrips();
    });
    this.loadMap();
    this.listenPos()
    this.setTokenFCM()
   
  }

  startPosition:any;


  // GOOGLE AUTOCOMPLETE

  updateSearchResultsStart(){
    if (this.autocompleteStart.input == '') {
      this.autocompleteItemsStart = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocompleteStart.input },
    (predictions, status) => {
      this.autocompleteItemsStart = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItemsStart.push(prediction);
        });
      });
    });    
  }
  updateSearchResults(){
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
    (predictions, status) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          this.autocompleteItems.push(prediction);
        });
      });
    });
  }
  selectSearchResult(item) {
    this.autocomplete.input = item.description;
    this.autocompleteItems = [];
    console.log(item)
    this.location = item
    this.placeid = this.location.place_id
    console.log('placeid'+ this.placeid)
    this.calculateAndDisplayRoute()
  }
  selectSearchResultStart(item) {
    this.autocompleteStart.input = item.description;
    this.autocompleteItemsStart = [];
    this.geocoder.geocode({placeId:item.place_id},((results,status)=>{
      this.startPosition = {
        lat: results[0].geometry.location.lat(),
        lng: results[0].geometry.location.lng()
      }
      console.log(this.startPosition);
    
    }))
    console.log(item);
    this.locationStart = item;
    this.placeidStart = this.locationStart.place_id;
    console.log('placeidStart '+ this.placeidStart)
    this.calculateAndDisplayRoute()
  }
  GoTo(){
    return window.location.href = 'https://www.google.com/maps/place/?q=place_id:'+this.placeid;
  }

  // GOOGLE AUTOCOMPLETE FINISHED

  searchTrips(){
    this.trips = [];
    let queryTrips = Date.now().toString+'queryPost'+Math.random();
    this._corek.socket.emit('query_post', { 'condition': {"post_status":0}, 'event':queryTrips});
    this._corek.socket.on(queryTrips, (trips) => {
      let locations = ' '; 
      if(trips.length > 0){
        for (let trip of trips){
          let directions = JSON.parse(trip.post_content)
          for (let direction of directions){
            locations += direction.name.substr(0,40)+' - ';
          }
          let dateTime = moment(trip.post_date_gmt).add(4, 'hours').format("HH:mm");
          this.trips.push({id:trip.ID ,title:trip.post_title, type:trip.post_type, point:locations, time:dateTime})
        }
        this.loader = false;
      }else{
        this.loader = false;
      }
    });
  }



  // FUNCTIONS OF GOOGLE MAPS API

  loadMap() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.whereIMlatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      
      let mapOptions = {
        center: latLng,
        disableDefaultUI: true,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      
 
      this.getAddressFromCoords(resp.coords.latitude, resp.coords.longitude);
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      this.directionsDisplay.setMap(this.map)

      const mySelfMarker = new google.maps.Marker({ 
        map       : this.map,
        animation : google.maps.Animation.DROP,
        position  : latLng,
        title     : 'Yo'
       });
 
      this.map.addListener('tilesloaded', () => {
        //'accuracy',this.map);
        this.getAddressFromCoords(this.map.center.lat(), this.map.center.lng())
      });
 
    }).catch((error) => {
      //'Error getting location', error);
    });
  }
 
  listenPos(){
    let watch = this.geolocation.watchPosition();
  watch.subscribe((data) => {
   // data can be a set of coordinates, or an error (if an error occurred).
   // data.coords.latitude
   // data.coords.longitude
   
    let body = {
      lat: data.coords.latitude,
      lng:data.coords.longitude
    }
   this.pasajeroService.setPasajeroPosition(localStorage.getItem('id'),body).subscribe((resp)=>{
    
   })
  
  });
  }

  getDistancia(){
    this.pasajeroService.getDistancia().subscribe((resp)=>{
      console.log(resp)
    })
  }

  setTokenFCM(){
    this.pasajeroService.setPasajeroTokenFCM(localStorage.getItem('id'),localStorage.getItem('token')).subscribe()
  }

  getAddressFromCoords(lattitude, longitude) {
    console.log("getAddressFromCoords "+lattitude+" "+longitude);
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
 
    this.nativeGeocoder.reverseGeocode(lattitude, longitude, options)
      .then((result: NativeGeocoderResult[]) => {
        console.log('entre')
        this.address = "";
        let responseAddress = [];
        for (let [key, value] of Object.entries(result[0])) {
          if(value.length>0)
          responseAddress.push(value);
 
        }
        responseAddress.reverse();
        for (let value of responseAddress) {
          this.address += value+", ";
        }
        this.address = this.address.slice(0, -2);
        console.log('no address',this.address)
        this.whereIAM = this.address
      })
      .catch((error: any) =>{ 
        this.address = "Address Not Available!";
      });
  }

  // RESERVE TRIPS

  reserveTrip(ID, boton){
    if(boton === 'programar'){
      //console.log('Pulso Programar')
      localStorage.setItem('tab1-turisviaje-id', ID);
      localStorage.setItem('tab1-touch', 'schedule');
      this.router.navigate(['/programar-viaje']);
    }else{
      //console.log('Pulso Tomar')
      localStorage.setItem('tab1-turisviaje-id', ID);
      localStorage.setItem('tab1-touch', 'take');
      this.router.navigate(['/programar-viaje']);
    }
  }

  // TRAZAR RUTA

  calculateAndDisplayRoute() {
    const that = this;
    this.directionsService.route({
      origin: this.locationStart.description,
      destination: this.location.description,
      travelMode: google.maps.TravelMode.DRIVING
    }, (response, status) => {
      console.log(this.locationStart)

      if (status === 'OK') {
        that.directionsDisplay.setDirections(response);
        console.log(response.routes[0].legs[0].distance)
        console.log(response.routes[0].legs[0].duration)

        // CALCULO PARA EL COSTO

        let distancia = response.routes[0].legs[0].distance.value;
        let duracion = response.routes[0].legs[0].duration.value;

        let precio = 10;

        let tarifa_distancia = (distancia / 1000) * 3;
        let tarifa_duracion = (duracion * 0.0225);

        precio += tarifa_distancia + tarifa_duracion;


        this.precio = precio.toFixed(2),'$';

        this.pedir_viaje = true;
        this.presentAlert();


      } else {
        this.errorRoute();
      }
    });
  }

  async errorRoute(){
    const alert = await this.alertController.create({
      header: 'No se encontro una ruta valida.',
      subHeader: 'Ingrese otro lugar cercano.',
      buttons: [
        {
          text: 'Aceptar',
          role: 'agree',
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Seleccione el idioma de su conductor',
      subHeader: '',
      inputs: [
        {
          name: 'idioma',
          type: 'radio',
          label: 'Español',
          value: 'spanish',
          checked: true
        },
        {
          name: 'idioma',
          type: 'radio',
          label: 'Ingles',
          value: 'english',
          checked: false
        },
      ],
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          handler: (input) => {
            this.languaje = input;
          }
        }
      ]
    });
    await alert.present();
  }

  async pedirViaje() {
    const alert = await this.alertController.create({
      header: '¿Esta seguro que desea realizar este viaje?',
      subHeader: '',
      buttons: [
        {
          text: 'Pedir Viaje',
          role: 'ok',
          handler: () => {
            function randomString(length, chars) {
              var result = '';
              for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
              return result;
            } 

            // let codStart: number  = Math.random() * 100000;
            // let codEnd: number = Math.random() * 100000;
            const codStart: string  = randomString(4, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');;
            const codEnd: string = randomString(4, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');;

            this.estiloViajeEnviado  = 'estiloDesactivado';
            var day = new Date();
            var date = new Date(day).getTime();
            var now = new Date().getTime(); 
            
                // Mandar a corek, viaje programado
                let walletSql = 'SELECT cash FROM wp_users WHERE user_login = "'+localStorage.getItem('email')+'";';
                let walletEvent = Date.now()+'taxi_trip'+Math.random();
                this._corek.socket.emit('query',{'event':walletEvent, 'querystring':walletSql});
                this._corek.socket.on(walletEvent, (responses)=>{ 
                  if(this.precio > responses[0].cash){
                    this.pleaseRecharge();
                  }else{
                    let cobrarSql = "UPDATE wp_users SET cash = (cash - "+ this.precio +") WHERE user_login = '"+ localStorage.getItem('email') +"';";
                    let cobrarSqlEvent = Date.now()+'taxi_trip'+Math.random();
                    this._corek.socket.emit('query',{'event':cobrarSqlEvent, 'querystring':cobrarSql});
                    this._corek.socket.on(cobrarSqlEvent, (responses)=>{ 
                      let scheduleTripSql = 'INSERT INTO taxi_trips (client, time, precio, startID, endID)';
                          scheduleTripSql += 'VALUES ("'+ localStorage.getItem('email') +'", "'+ day +'","'+ this.precio +'","'+ this.locationStart.description+'","'+ this.location.description +'");';
                      let scheduleTripEvent = Date.now()+'taxi_trip'+Math.random();
                      this._corek.socket.emit('query',{'event':scheduleTripEvent, 'querystring':scheduleTripSql});
                      this._corek.socket.on(scheduleTripEvent, (responses)=>{ 
                        console.log(responses);
                        let tripId = responses.insertId
                        let walletHistorySql = 'INSERT INTO wallet_history (user, monto, tipo, fecha) VALUES ("'+ localStorage.getItem('email') +'", "'+ this.precio +'", "'+ 'Viaje Taxi' +'", "'+ Date.now() +'");';
                        let walletHistoryEvent = Date.now()+'taxi_trip'+Math.random();
                        this._corek.socket.emit('query',{'event':walletHistoryEvent, 'querystring':walletHistorySql});
                        this._corek.socket.on(walletHistoryEvent, (responses)=>{ 
                          let codSql = 'INSERT INTO codes (trip_id, trip_type, cod_init, cod_end, dri_cod_init, dri_cod_end)'; 
                          codSql += 'VALUES ("'+ tripId +'","'+ 'taxi' +'","'+ codStart +'","'+ codEnd +'","'+ false +'","'+ false +'");';
                          let codEvent = Date.now()+'taxi_trip'+Math.random();
                          this._corek.socket.emit('query',{'event':codEvent, 'querystring':codSql});
                          this._corek.socket.on(codEvent, (responses)=>{ 
                            console.log(responses);
                            this.pasajeroService.alertToDriversNear(this.startPosition,tripId).subscribe()
                            this.router.navigate(['/tabs/tab2']);
                            this.estiloViajeEnviado  = '';
                          });
                        });
                      });
                    });
                  }
                });
          }
        },{
          text: 'Cancelar',
          role: 'bad',
          handler: () => {
          
         }
        }
      ]
    });
    await alert.present();
  }

  async pleaseRecharge(){
    const alert = await this.alertController.create({
      header: 'Porfavor recargue para poder solicitar su viaje.',
      subHeader: '',
    });

    await alert.present();
  }
  
}
