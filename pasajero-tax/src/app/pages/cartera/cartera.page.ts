import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CorekService } from '../../services/corek.service';

@Component({
  selector: 'app-cartera',
  templateUrl: './cartera.page.html',
  styleUrls: ['./cartera.page.scss'],
})
export class CarteraPage {
  cargado;
  wallet_history = [];

  accion = 'cartera';
  cash = 0;

  ionViewWillEnter(){
    this.actualizar()
  }
  constructor(
    private alertCtrl: AlertController,
    public _corek:CorekService
    ) {
      this.actualizar();
    }

  async pagarStripe() {
    const alert = await this.alertCtrl.create({
      header: 'Sumar saldo',
      inputs: [
        {
          name: 'monto',
          placeholder: 'Monto',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Recargar',
          handler: data => {
            if (data.monto !== '' ) {
              console.log(data.monto)
              let sql2 = "UPDATE wp_users SET cash = (cash + "+ data.monto +") WHERE user_login = '"+ localStorage.getItem('email') +"';"
              let sql = 'INSERT INTO wallet_history (user, monto, tipo, fecha) VALUES ("'+ localStorage.getItem('email') +'", "'+ data.monto +'", "'+ 'Recarga' +'", "'+ Date.now() +'");';
              let login = Date.now()+'Login'+Math.random();
              this._corek.socket.emit('query',{'event':login, 'querystring':sql});
              this._corek.socket.emit('query',{'event':login+'ss', 'querystring':sql2});

              this._corek.socket.on(login, (responses)=>{ console.log(responses)});
              this._corek.socket.on(login+'ss', (responses)=>{ console.log(responses); this.actualizar();});
              this.actualizar();
            } else {
              console.log('error')
              return false;
            }
          }
        }
      ]
    });
    await alert.present();
  }

  async retirarSaldo(){
    const alert = await this.alertCtrl.create({
      header: 'Solicitud Retirar Saldo',
      inputs: [
        {
          name: 'monto',
          placeholder: 'Monto',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Retirar',
          handler: data => {
            if (data.monto !== '' ) {
              
              let sql = 'INSERT INTO wallet_history (user, monto, tipo, fecha) VALUES ("'+ localStorage.getItem('email') +'", "'+ data.monto +'", "'+ 'Solicitud Retiro' +'", "'+ Date.now() +'");';
              let login = Date.now()+'Login'+Math.random();
              this._corek.socket.emit('query',{'event':login, 'querystring':sql});
              this._corek.socket.on(login, (responses)=>{ console.log(responses)});
              let sql2 = "UPDATE wp_users SET cash = (cash + "+ data.monto +") WHERE user_login = '"+ localStorage.getItem('email') +"';"
              this._corek.socket.emit('query',{'event':login+'ss', 'querystring':sql2});
              this._corek.socket.on(login+'ss', (responses)=>{ console.log(responses); this.actualizar();});
            } else {
              console.log('error')
              return false;
            }
          }
        }
      ]
    });
    await alert.present();

  }


  actualizar(){
    this.cargado = false;
    let sql = 'SELECT cash FROM wp_users WHERE user_login = "'+localStorage.getItem('email')+'";';
    let login = Date.now()+'Loginaa'+Math.random();
    this._corek.socket.emit('query',{'event':login, 'querystring':sql});
    this._corek.socket.on(login, (responses)=>{ this.cash = responses[0].cash; this.cargado = true});

    let sql2 = 'SELECT * FROM wallet_history WHERE user = "'+ localStorage.getItem('email') +'";';
    let login2 = Date.now()+'Loginaaa'+Math.random();
    this._corek.socket.emit('query',{'event':login2, 'querystring':sql2});
    this._corek.socket.on(login2, (responses)=>{ this.wallet_history = responses });

    console.log(this.cash)

    //this.wallet_history.sort((a,b)=>a.fecha-b.fecha);
  }
}
