import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { ModalPage } from '../../pages/modal/modal.page';
import { CorekService } from '../../services/corek.service';

@Component({
  selector: 'app-restore-pass',
  templateUrl: './restore-pass.page.html',
  styleUrls: ['./restore-pass.page.scss'],
})
export class RestorePassPage implements OnInit {

  restoreForm: FormGroup;

  constructor(
    public form:FormBuilder, 
    public loadingController: LoadingController,
    public toastController: ToastController,
    public modalController: ModalController,
    private _corek:CorekService
  ) { 

    this.restoreForm = form.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
    });

  }

  ngOnInit() {
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async restore(){
    
    // Modal
    const modal = await this.modalController.create({
      component: ModalPage,
      cssClass: 'my-custom-modal-css',
      componentProps: {
        'image': 'icono',
        'title': 'Restablecer contraseña',
        'content': 'Hemos enviado exitosamente un link a su correo para restablecer su contraseña'
      }
    });
    // Loading
    const loading = await this.loadingController.create({
      duration: 15000,
      message: 'Espere por favor...',
      translucent: true,
    });
    loading.present();
    // coneccion
    let conection = Date.now().toString+'conection_restore'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      // obtener usuario
      let getUser = Date.now().toString() +'get_user_retore'+Math.random();
      this._corek.socket.emit('get_users', {'condition':{'user_email':this.restoreForm.value.email}, 'event':getUser}); 
      this._corek.socket.on(getUser, (users)=>{
        if(users.length>0){
          // Configuracion de smtp
          this._corek.socket.emit('confsmtp',{'token':1});
          this._corek.socket.on('confsmtp', (responseMail)=>{
            // Envio de correo
            this._corek.socket.emit('forgot_password',{"email":this.restoreForm.value.email});
            this._corek.socket.on("forgot_password", (responseSend)=>{
              if(responseSend.message == true){
                loading.dismiss();
                modal.present();
              }else{ 
                loading.dismiss();
                this.presentToast('No se pudo enviar el correo.');
              }
            });
          });
        }else{
          this.presentToast('El correo no se encuentra registrado.');
        }
      });
    });
  }

}
