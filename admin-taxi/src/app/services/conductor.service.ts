import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConductorService {

constructor(public http:HttpClient) { }
// URL_SERVICIOS = 'http://localhost:3000'
URL_SERVICIOS = 'http://167.172.136.188:3000'
getConductores(){
  return this.http.get(`${this.URL_SERVICIOS}/usuario/conductores`)
}


}
