import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router) { }

  login(ID){
    localStorage.setItem('ID', ID);
    this.router.navigate(['/dashboard']);
  }

  logout(){
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
