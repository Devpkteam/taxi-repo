import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { CorekService } from '../../services/corek.service';
import { ToastrService } from 'ngx-toastr';



declare const google: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

    private map: any;
    private userPolyLine: any[] = [];
    private userMarkers: any[] = [];                                                                                                                                                                                                                                                                                                                                                          
    private userPoints: any[] = [];
    private userPlaces: any = [];                                                                                                                       
    public userInputs: any = [];
    public time;
    public contador = 0;
    public waypoints: any = []
    public duration: any;
    public title: String;
    // service
    public directionsService = new google.maps.DirectionsService();
    public directionsRenderer = new google.maps.DirectionsRenderer();

    constructor(
      private _corek:CorekService,
      private toastr: ToastrService,
      private httpClient: HttpClient,       
    ){}

    ngOnInit() {

      let conection = Date.now().toString+'conection'+Math.random();
      this._corek.ConnectCorekconfig(conection);
      this._corek.socket.on(conection, (response)=>{
        console.log(response);
      });

      // Create maps      
      this.map = new google.maps.Map(document.getElementById('map'), {
        zoom: 5,
        center: {lat: 19.4326009, lng: -99.1333416}
      });
    
      let map = this.map;
    
      map.addListener('click', (e) => {
        
        // Puntos
        let points = this.userPoints;

        let address = e.latLng.lat()+','+e.latLng.lng();

        let route = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=AIzaSyDO9BQyVEL9xHKqBefB45Ml8GRCpsxj7co`;
  
        this.httpClient.get<any>(route).subscribe(res => {
          this.userPlaces.push({name:res.results[0].formatted_address,lat:e.latLng.lat(), lng:e.latLng.lng()});
          if(this.userPlaces.length >=2){
            this.drawRoute();
          }
        });

        if(this.userPlaces.length < 2){
          var marker = new google.maps.Marker({
            position: e.latLng,
            map: map
          });
          
          // Insertar marcadores
          this.userMarkers.push(marker);
        }

        // Marcadores

        // if (points.length !== 0) {
        //   let path = [
        //     points[points.length - 1],
        //     e.latLng
        //   ];
    
        //   let poliLine = new google.maps.Polyline({
        //     path,
        //     geodesic: true,
        //     strokeColor: '#1f2122',
        //     strokeOpacity: 1.0,
        //     strokeWeight: 5
        //   });
    
        //   poliLine.setMap(map);
        //   this.userPolyLine.push(poliLine);
        // }
    
        // points.push(e.latLng);

      });
    }

    async presentToast(icon, message, color, duration){
      await this.toastr.info('<span class="now-ui-icons '+icon+'"></span> '+message, '', {
        timeOut: duration,
        closeButton: true,
        enableHtml: true,
        toastClass: "alert alert-"+color+" alert-with-icon",
        positionClass: 'toast-bottom-center'
      });
    }
    
    getRoute() {
      console.log(this.title);
      this.presentToast('loader_refresh spin', 'Espere por favor', 'danger', 2000);
      let now = new Date();
      let date = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
      let dateTrip = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+' '+this.time+':00';
      let insertTrip = Date.now()+'inserPost'+Math.random();
      this._corek.socket.emit('insert_post',{'condition':{'post_type':'turis-trips', 'post_date':date, 'post_date_gmt':dateTrip,'post_status':0, 'post_title':this.title,'post_content': JSON.stringify(this.userPlaces)},'event':insertTrip});
      this._corek.socket.on(insertTrip, (response)=>{
        this.presentToast('location_pin', 'Viaje turistico creado', 'info', 4000);
        this.clearMap();
      });  
    }
    
    drawRoute(){
      
      let estimateDuration;
      estimateDuration = 'Espere';

      this.directionsRenderer.setMap(null);
      this.waypoints = [];


      this.userMarkers.forEach((item, index) => {
        item.setMap(null);
      });
      
      let points = this.userPlaces.slice(1, this.userPlaces.length-1)
      points.forEach(element => {
        this.waypoints.push({location:{lat:element.lat, lng:element.lng}, stopover: true})
      });

      console.log(this.waypoints);

      this.directionsRenderer.setMap(this.map);
    
      this.directionsService.route({
        origin: {lat:this.userPlaces[0].lat, lng:this.userPlaces[0].lng},
        destination: {lat:this.userPlaces[this.userPlaces.length-1].lat, lng:this.userPlaces[this.userPlaces.length-1].lng},
        waypoints: this.waypoints,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      }, (response, status) => {
        console.log(response, status)
        if (status === 'OK') {
          estimateDuration = 0;
          for (let leg of response.routes[0].legs){
            console.log(leg.duration);
            estimateDuration+= leg.duration.value;
          }
          let hours = Math.floor( estimateDuration / 3600 );  
          let minutes = Math.floor( estimateDuration / 60 );
          let seconds = estimateDuration % 60;
          //Anteponiendo un 0 a los minutos si son menos de 10 
          let minutes1 = minutes < 10 ? '0' + minutes : minutes;
          //Anteponiendo un 0 a los segundos si son menos de 10 
          let seconds1 = seconds < 10 ? '0' + seconds : seconds;

          this.duration = hours + ":" + minutes1 + ":" + seconds1;
          
          this.directionsRenderer.setDirections(response);
        } else {
          this.presentToast('', 'Ocurrio un error, intente de nuevo', 'danger', 2000);
        }
      });
    }

    clearMap() {
      this.directionsRenderer.setMap(null);
      this.waypoints = [];

      this.userMarkers.forEach((item, index) => {
        item.setMap(null);
      });

      this.userPolyLine.forEach((item, index) => {
        item.setMap(null);
      });
  
      this.userInputs = [];
      this.userPoints = [];
      this.userMarkers = [];
      this.userPlaces = [];
      this.time = '';
    }

    addInput(){
      this.userInputs.length++;
    }

    recieveDirection($event) {
      console.log($event);
      var marker = new google.maps.Marker({
        position: {lat: $event.lat ,lng: $event.lng},
        map: this.map
      });
      // Insertar marcadores
      this.userMarkers.push(marker);
      this.userPlaces.push($event);
      if(this.userPlaces.length >=2){
        this.drawRoute();
      }
    }
    

}
