import { Component, OnInit, NgZone, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
declare var google;


@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  @Output() directionEmitter = new EventEmitter<any>()

  public showList = false;
  public inpust = [];
  public service = new google.maps.places.AutocompleteService();
  public autocompleteItems = [];
  public form: FormGroup;
  public value;

  constructor(
    public formBuilder:FormBuilder,
    private zone: NgZone,
  ) {

    this.form = formBuilder.group({
      direction: ['', [Validators.required]],
    });

  }

  ngOnInit() {
  }

  getAddress(){
    this.showList = true;
    if (this.form.value.direction == '') {
      this.autocompleteItems = [];
      return;
    }

    let me = this;
    this.service.getPlacePredictions({input: this.form.value.direction ,
    componentRestrictions: {country: 'mx'}}, (predictions, status) => {
      me.autocompleteItems = [];
 
      me.zone.run(() => {
        if (predictions != null) {
          predictions.forEach((prediction) => {
            me.autocompleteItems.push(prediction.description);
          });
        }
      });
    });
  }

  selectPlace_begin(item){
    this.showList = false;
    this.value = item;
    this.geoCode(item);
  }

  geoCode(address:any) { 
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address, }, (results, status) => {
      let direction = {name:address, lat:results[0].geometry.location.lat(), lng: results[0].geometry.location.lng()}
      this.directionEmitter.emit(direction);
    });
  } 

}
