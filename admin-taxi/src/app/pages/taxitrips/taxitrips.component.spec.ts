import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxitripsComponent } from './taxitrips.component';

describe('TaxitripsComponent', () => {
  let component: TaxitripsComponent;
  let fixture: ComponentFixture<TaxitripsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxitripsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxitripsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
