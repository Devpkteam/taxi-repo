import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';
import { CorekService } from '../../../services/corek.service';
import { ShowImageComponent } from '../show-image/show-image.component';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.scss']
})
export class UsersDetailsComponent implements OnInit {

  displayedColumns: string[] = ['name', 'number', 'front', 'back'];
  dataSource: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  public userData: any;
  public url: any;
  public metaData: any[] = [];
  public showBar = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<UsersDetailsComponent>,
    private _corek:CorekService,
    public dialog: MatDialog
    ){ 
      this.userData = data;
      this.url = this._corek.urlUploads;
    }

  ngOnInit() {
    if(this.userData.rol == 'Conductor'){
      let getUserMeta = Date.now()+'getUserMeta'+Math.random();
      this._corek.socket.emit('get_user_meta',{"condition":{"user_id":this.userData.id}, 'event':getUserMeta});
      this._corek.socket.on(getUserMeta,(metas)=> {
        if(metas){
          for(let meta of metas){
            if(meta.meta_key != 'referencia'){
              let metaJson = {name:meta.meta_key, number:meta.meta_value, front:meta.document_front, back:meta.document_back}
              this.metaData.push(metaJson);
            }
          }
          console.log(this.metaData);
          this.dataSource = new MatTableDataSource(this.metaData);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }
        this.showBar = false;
      });
    }else{
      this.showBar = false;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  openDialog(image): void {

    console.log(image);
    const dialogRef = this.dialog.open(ShowImageComponent, {
      width: '50%',
      data: image
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
