import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';


@Component({
  selector: 'app-trips-details',
  templateUrl: './trips-details.component.html',
  styleUrls: ['./trips-details.component.scss']
})
export class TripsDetailsComponent implements OnInit {

  displayedColumns: string[] = ['date', 'title', 'points', 'time', 'status'];
  dataSource: any;
  dataTrips: any = [];
  points: any = [];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    @Inject(MAT_DIALOG_DATA) data,
    public dialogRef: MatDialogRef<TripsDetailsComponent>,
  ) { 
    console.log(data);
    this.dataTrips = data
    this.points = data.points
  }
  
  ngOnInit() {
    // this.dataSource = new MatTableDataSource(this.dataTrips);
    console.log(this.dataTrips.date, this.points)
    // this.dataSource = new MatTableDataSource(this.metaData);
    // this.dataSource.sort = this.sort;
    // this.dataSource.paginator = this.paginator;
  }

}
