import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CorekService } from '../../services/corek.service';

import {} from 'googlemaps';
import { ConductorService } from '../../services/conductor.service';

@Component({
  selector: 'app-mapa-conductores',
  templateUrl: './mapa-conductores.component.html',
  styleUrls: ['./mapa-conductores.component.scss']
})
export class MapaConductoresComponent implements OnInit {



constructor(public conductorService:ConductorService){

}
@ViewChild('map', {static: false}) mapElement: any;

map: google.maps.Map;

ngOnInit() {
  console.log('hola')
  const mapProperties = {
       center: new google.maps.LatLng(35.2271, -80.8431),
       zoom: 15,
       mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  this.map = new google.maps.Map(document.getElementById('map'), mapProperties);

  this.getConductores();

//   var marker = new google.maps.Marker({
//     position: {lat: 35.2371 ,lng:  -80.8331},
//     map: this.map,
    
//     label: 'David Valor',
//     icon: '../../assets/iconos/taxi.png'
//   });

//   var marker2 = new google.maps.Marker({
//     position: {lat: 35.2351 ,lng:  -80.8331},
//     map: this.map,
//     icon: '../../assets/iconos/taxi.png',
//     label:'Jogeiker Lizarraga'
//   });
// let lng = -80.8331;
// let lat2 =  35.2351;
//   setInterval(() => {
//     lng += 0.0001;
//     lat2 += 0.0001;
//     marker.setPosition({lat: 35.2351 ,lng})
//     marker2.setPosition({lat: lat2 ,lng: -80.8331})

//   }, 500);


 
}



markers = [];
setPosConductores(conductores){
  this.deleteMarkes()



 
  conductores.forEach((conductor) => {
   
    let icon_route;
    if(conductor.alert == 'true'){
      
      icon_route = '../../assets/iconos/taxi_rojo.png'
    }else{
      icon_route = '../../assets/iconos/taxi_azul.png'

    }
    
  var  marker = new google.maps.Marker({
          position: {lat: Number( conductor.lat) ,lng: Number(conductor.lng) },
          map: this.map,
          
          label: conductor.display_name,
          icon:  icon_route
        });
        this.markers.push(marker);
  });

  

  
}


deleteMarkes(){
  this.markers.forEach((marker)=>{
    marker.setMap(null);
  })
  this.markers.length = 0;

}


getConductores(){
  setInterval(()=>{
    this.conductorService.getConductores().subscribe((resp:any)=>{
      if(resp.ok){
       
        this.setPosConductores(resp.conductores)
      }
    })

   },1000)
}

  
  

}
