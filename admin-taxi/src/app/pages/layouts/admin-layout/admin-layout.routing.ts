import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { MapsComponent } from '../../maps/maps.component';
import { UsersComponent } from '../../users/users.component';
import { TripsComponent } from '../../trips/trips.component';
import { DriversComponent } from '../../drivers/drivers.component';
import { AutocompleteComponent } from '../../maps/autocomplete/autocomplete.component';
import { PaymentsComponent } from '../../payments/payments.component';
import { MapaConductoresComponent } from '../../mapa-conductores/mapa-conductores.component';
import { TaxitripsComponent } from '../../../pages/taxitrips/taxitrips.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent},
    { path: 'create-trip',    component: MapsComponent },
    { path: 'users',          component: UsersComponent},
    { path: 'drivers',        component: DriversComponent },
    { path: 'trips',          component: TripsComponent},
    { path: 'payments',       component: PaymentsComponent},
    { path: 'drivers-maps',       component: MapaConductoresComponent},
    { path: 'taxi',       component: TaxitripsComponent},
];
