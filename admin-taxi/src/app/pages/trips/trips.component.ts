import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator, MatDialog} from '@angular/material';
import { CorekService } from '../../services/corek.service';
import { TripsDetailsComponent } from '../dialogs/trips-details/trips-details.component';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {

  public showBar = true;
  displayedColumns: string[] = ['position', 'fecha', 'titulo', 'hora', 'conductor', 'tipo', 'estatus', 'opciones'];
  dataSource: any;
  trips_element = [];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private _corek:CorekService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.getTrips();
    });
  }

  getTrips(){
    this.trips_element = [];
    let getTrips = Date.now().toString() +'get_trips'+Math.random();
    this._corek.socket.emit('query',{'event':getTrips, 'querystring':"SELECT * FROM wp_posts WHERE post_status != -1"});;
    this._corek.socket.on(getTrips, (trips)=>{
      if(trips.length>0){
        trips.forEach((trip, index) => {
          let fechaDB = new Date(trip.post_date_gmt);
          let datetime = fechaDB.setHours(fechaDB.getHours()+6);
          this.trips_element.push({id:trip.ID, position:index+1, date:trip.post_date, points:JSON.parse(trip.post_content) ,title:trip.post_title, time:datetime, status:trip.post_status, type:trip.post_type});
        });
        this.dataSource = new MatTableDataSource(this.trips_element);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.showBar = false;
      }else{
        this.showBar = false;
      }
    });
  }

  changeStatus(ID, option){
    let newStatus
    switch(option){
      case 'activate':
        newStatus = 0;
      break;
      case 'desactivate':
        newStatus = 3;
      break;
      case 'delete':
        newStatus = -1;
      break;
    }
    let updateTrips = Date.now().toString+'updateTrips'+Math.random();
    this._corek.socket.emit("update_post",{"set":{'post_status':newStatus},"condition":{"ID":ID}, 'event':updateTrips});
    this._corek.socket.on(updateTrips, (response) => {
      this.getTrips();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(trip): void {
    const dialogRef = this.dialog.open(TripsDetailsComponent, {
      width: '50%',
      data: trip
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}
